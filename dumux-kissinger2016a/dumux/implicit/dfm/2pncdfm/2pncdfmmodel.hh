// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Base class for all models which use the two-phase,
 *        n-component discrete fracture fully implicit model.
 *        Adaption of the fully implicit scheme to the two-phase n-component discrete fracture flow model.
 */

#ifndef DUMUX_TWOPNC_DFM_MODEL_HH
#define DUMUX_TWOPNC_DFM_MODEL_HH

#include <dune/common/version.hh>

#include <dumux/implicit/common/implicitvelocityoutput.hh>
#include <dumux/implicit/2pnc/2pncmodel.hh>
#include <dumux/implicit/2pnc/2pncproperties.hh>
#include "2pncdfmproperties.hh"



namespace Dumux
{

/*!
 * \ingroup TwoPNCDFMModel
 * \brief Adaption of the fully implicit scheme to the two-phase n-component discrete fracture flow model.
 *
*/

template<class TypeTag >
class TwoPNCDFMModel : public TwoPNCModel<TypeTag>
{

};
}

#include "2pncdfmpropertydefaults.hh"
#include <dumux/implicit/dfm/common/dfmpropertydefaults.hh>

#endif
