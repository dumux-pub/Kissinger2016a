/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase n component discrete fracture-matrix model.
 */
#ifndef DUMUX_MODELS_2PNCDFM_VOLUME_VARIABLES_HH
#define DUMUX_MODELS_2PNCDFM_VOLUME_VARIABLES_HH

#include <dune/common/fvector.hh>

#include <dumux/implicit/dfm/common/dfmvolumevariables.hh>
#include <dumux/material/fluidstates/compositionalfluidstate.hh>
//#include <dumux/implicit/2pnc/2pncindices.hh>
#include "2pncdfmproperties.hh"


namespace Dumux
{
/*!
 * \ingroup TwoPNCDFMModel
 * \ingroup ImplicitVolumeVariables
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase discrete fracture-matrix model.
 */
template <class TypeTag>
class TwoPNCDFMVolumeVariables : public DFMVolumeVariables<TypeTag>
{
    typedef DFMVolumeVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, MatrixVolumeVariables) MatrixVolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,
    };

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) GridType;
    typedef typename GridType::ctype DT;

    enum {
            dim = GridView::dimension,
            dimWorld = GridView::dimensionworld
    };
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename Dune::ReferenceElements<DT, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<DT, dim> ReferenceElement;
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

public:
    /*!
     * \copydoc ImplicitVolumeVariables::update
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {
        ParentType::update(priVars,
                           problem,
                           element,
                           fvGeometry,
                           scvIdx,
                           isOldSol);

        //here the fluidstate is overwritten, the same function has already been called in the ParentType::update
//        this->completeFluidState(priVars, problem, element, fvGeometry, scvIdx, fluidState_);
//        fluidState_ = *MatrixVolumeVariables::fluidState(); //get reference and derefernce to get the value again
        const FluidState fs = MatrixVolumeVariables::fluidState(); //get const reference
        fluidState_ = fs; //copy by value

        const MaterialLawParams &materialParams =
            problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);

        mobilityMatrix_[wPhaseIdx] =
            MaterialLaw::krw(materialParams, fluidState_.saturation(wPhaseIdx))
            / fluidState_.viscosity(wPhaseIdx);

        mobilityMatrix_[nPhaseIdx] =
            MaterialLaw::krn(materialParams, fluidState_.saturation(wPhaseIdx))
            / fluidState_.viscosity(nPhaseIdx);

        asImp_().updateFracture(priVars, problem, element, fvGeometry, scvIdx, isOldSol);
    }

    /*!
     * \brief Construct the volume variables for all fracture vertices.
     *
     * \param priVars Primary variables.
     * \param problem The problem which needs to be simulated.
     * \param element The DUNE Codim<0> entity for which the volume variables ought to be calculated
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx Sub-control volume index
     * \param isOldSol Tells whether the model's previous or current solution should be used.
     */
    void updateFracture(const PrimaryVariables &priVars,
                        const Problem &problem,
                        const Element &element,
                        const FVElementGeometry &fvGeometry,
                        int scvIdx,
                        bool isOldSol)
    {

            const MaterialLawParams &materialParamsMatrix =
                    problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);

            satWMatrix_ = fluidState_.saturation(wPhaseIdx);
            satNMatrix_ = fluidState_.saturation(nPhaseIdx);

            ///////////////////////////////////////////////////////////////////////////////
            if (this->fractureScv().isFractureScv)
            {
                int dofIdxGlobal = problem.model().dofMapper().subIndex(element, scvIdx, dofCodim);
                int phasePresence = problem.model().phasePresence(dofIdxGlobal, isOldSol);
                fluidStateFracture_ = fluidState_;
                const MaterialLawParams &materialParamsFracture =
                        problem.spatialParams().fractureMaterialLawParams(element, fvGeometry, scvIdx);

                if(phasePresence == bothPhases)
                {
                    if(formulation == plSg)
                    {
                        satNFracture_  = priVars[switchIdx];
                        satWFracture_  = 1.0 - satNFracture_;
                    }
                    else if(formulation == pgSl)
                    {
                        satWFracture_  = priVars[switchIdx];
                        satNFracture_  = 1.0 - satNFracture_;
                    }
                    else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");

                    pcFracture_ = MaterialLaw::pc(materialParamsFracture, satWFracture_);
                    Scalar pEntryMatrix = MaterialLaw::pc(materialParamsMatrix, 1);

                    //use interface condition - extended capillary pressure interface condition
                    if (problem.useInterfaceCondition())
                    {
                        interfaceCondition(materialParamsMatrix, pEntryMatrix);
                    }

                    fractureMobility_[wPhaseIdx] =
                            MaterialLaw::krw(materialParamsFracture, fluidStateFracture_.saturation(wPhaseIdx))
                    / fluidStateFracture_.viscosity(wPhaseIdx);

                    fractureMobility_[nPhaseIdx] =
                            MaterialLaw::krn(materialParamsFracture, fluidStateFracture_.saturation(wPhaseIdx))
                    / fluidStateFracture_.viscosity(nPhaseIdx);

                    // derivative resulted from BrooksCorey pc_Sw formulation
                    //            dsm_dsf_ = (1 - problem.spatialParams().swrm_) / (1 - problem.spatialParams().swrf_)
                    //                    * pow((problem.spatialParams().pdm_/ problem.spatialParams().pdf_),problem.spatialParams().lambdaM_)
                    //                    * (problem.spatialParams().lambdaM_ / problem.spatialParams().lambdaF_)
                    //                    * pow((satWFracture_ - problem.spatialParams().swrf_ ) / (1 - problem.spatialParams().swrf_),
                    //                            (problem.spatialParams().lambdaM_ / problem.spatialParams().lambdaF_) - 1);

                    // After modifying the Matrix saturations we have to update the matrix mobilities
                    mobilityMatrix_[wPhaseIdx] =
                            MaterialLaw::krw(materialParamsMatrix, satWMatrix_)
                    / fluidState_.viscosity(wPhaseIdx);

                    mobilityMatrix_[nPhaseIdx] =
                            MaterialLaw::krn(materialParamsMatrix, satWMatrix_)
                    / fluidState_.viscosity(nPhaseIdx);
                }
                else if (phasePresence == wPhaseOnly)
                {
                    satNFracture_ = fluidStateFracture_.saturation(nPhaseIdx);
                    satWFracture_ = fluidStateFracture_.saturation(wPhaseIdx);
                    pcFracture_ = MaterialLaw::pc(materialParamsFracture, satWFracture_);
                    fractureMobility_[wPhaseIdx] =
                            MaterialLaw::krw(materialParamsFracture, fluidStateFracture_.saturation(wPhaseIdx))
                    / fluidStateFracture_.viscosity(wPhaseIdx);

                    fractureMobility_[nPhaseIdx] =
                            MaterialLaw::krn(materialParamsFracture, fluidStateFracture_.saturation(wPhaseIdx))
                    / fluidStateFracture_.viscosity(nPhaseIdx);

                }
                else if (phasePresence == nPhaseOnly)
                {
                    satNFracture_ = fluidStateFracture_.saturation(nPhaseIdx);
                    satWFracture_ = fluidStateFracture_.saturation(wPhaseIdx);
                    pcFracture_ = MaterialLaw::pc(materialParamsFracture, satWFracture_);
                    fractureMobility_[wPhaseIdx] =
                            MaterialLaw::krw(materialParamsFracture, fluidStateFracture_.saturation(wPhaseIdx))
                    / fluidStateFracture_.viscosity(wPhaseIdx);

                    fractureMobility_[nPhaseIdx] =
                            MaterialLaw::krn(materialParamsFracture, fluidStateFracture_.saturation(wPhaseIdx))
                    / fluidStateFracture_.viscosity(nPhaseIdx);
                }
                else DUNE_THROW(Dune::InvalidStateException, "Phase presence: " << phasePresence << " is invalid.");
            }// end if (node)
            ///////////////////////////////////////////////////////////////////////////////
            else
            {
                /* the values of pressure and saturation of the fractures in the volumes where
                there are no fracture are set unphysical*/
                satNFracture_ = -1;
                satWFracture_ = -1;
                pcFracture_ = -1e100;
                fractureMobility_[wPhaseIdx] = 0.0;
                fractureMobility_[nPhaseIdx] = 0.0;
            }
    }

    /*!
     * \brief Extended capillary pressure saturation interface condition
     *
     * \param materialParamsMatrix the material law o calculate the sw as inverse of capillary pressure function
     *
     * This method is called by updateFracture
     */
    void interfaceCondition(const MaterialLawParams &materialParamsMatrix, Scalar pEntryMatrix)
    {
        /*2nd condition Niessner, J., R. Helmig, H. Jakobs, and J.E. Roberts. 2005, eq.10
        * if the capillary pressure in the fracture is smaller than the entry pressure
        * in the matrix than in the matrix
        * */
        if (pcFracture_ <= pEntryMatrix)
        {
            satWMatrix_ = 1.0;
            satNMatrix_ = 1 - satWMatrix_;
        }
        //3rd condition Niessner, J., R. Helmig, H. Jakobs, and J.E. Roberts. 2005, eq.10
        else
        {
            /*
             * Inverse capillary pressure function SwM = pcM^(-1)(pcF(SwF))
             */
            satWMatrix_ = MaterialLaw::sw(materialParamsMatrix, pcFracture_);
            satNMatrix_ = 1 - satWMatrix_;
        }
    }

    /*!
     * \brief Returns the effective saturation fracture of a given phase within
     *        the control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar saturationFracture(int phaseIdx) const
    {
        if (phaseIdx == wPhaseIdx)
            return satWFracture_;
        else
            return satNFracture_;
    }
    Scalar saturationMatrix(int phaseIdx) const
    {
         if (phaseIdx == wPhaseIdx)
             return satWMatrix_;
         else
             return satNMatrix_;
    }

    /*!
     * \brief Returns the effective mobility of a given phase within
     *        the control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar mobility(int phaseIdx) const
    { return mobilityMatrix_[phaseIdx]; }

    /*!
     * \brief Returns the effective mobility of a given phase within
     *        the control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar fractureMobility(int phaseIdx) const
    { return fractureMobility_[phaseIdx]; }

    /*!
     * \brief Returns the derivative dsm/dsf
     */
//    Scalar dsm_dsf() const
//    { return dsm_dsf_;}

protected:
    FluidState fluidState_;
    FluidState fluidStateFracture_;
    Scalar mobilityMatrix_[numPhases];
    Scalar fractureMobility_[numPhases];

    Scalar satW_;
    Scalar satWFracture_;
    Scalar satWMatrix_;
    Scalar satN_;
    Scalar satNFracture_;
    Scalar satNMatrix_;

    Scalar pc_;
    Scalar pcFracture_;
    Scalar pcMatrix_;
//    Scalar dsm_dsf_;

    bool isNodeOnFracture_;

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};
} // end namespace

#endif // DUMUX_MODELS_2PNCDFM_VOLUME_VARIABLES_HH
