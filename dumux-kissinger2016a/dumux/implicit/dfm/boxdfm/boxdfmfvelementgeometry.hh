// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Represents the finite volume geometry of a single element in
 *        the box scheme.
 */
#ifndef DUMUX_BOX_DFM_FVELEMENTGEOMETRY_HH
#define DUMUX_BOX_DFM_FVELEMENTGEOMETRY_HH

#include <dune/common/float_cmp.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/intersectioniterator.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/propertysystem.hh>
#include <dumux/implicit/box/boxproperties.hh>
#include <dumux/implicit/box/boxfvelementgeometry.hh>

namespace Dumux
{
namespace Properties
{
NEW_PROP_TAG(GridView);
NEW_PROP_TAG(Scalar);
NEW_PROP_TAG(ImplicitUseTwoPointFlux);
NEW_PROP_TAG(SpatialParams);
}

/*!
 * \ingroup BoxModel
 * \brief Represents the face of a fracture between two vertices in the
 * box scheme.
 *
 * This class contains all the information for the geometry of a fracture
 * sub-control volume face. The fracture is a connection between two
 * nodes with a virtual volume and a virtual width. The normal vector
 * of the fracture ScvFace is assumed to be parallel to the connecting
 * vector of the two vertices. Two-point flux is assumed between the two
 * the vertices.
 */
template<class TypeTag>
class BoxDFMFVElementGeoemtry : public BoxFVElementGeometry<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{dimWorld = GridView::dimensionworld};

    typedef BoxDFMFVElementGeoemtry<TypeTag> ThisType;
    typedef BoxFVElementGeometry<TypeTag> ParentType;

    enum{maxNC = (dim < 3 ? 4 : 8)};
    enum{maxNE = (dim < 3 ? 4 : 12)};
    enum{maxNF = (dim < 3 ? 1 : 6)};
    enum{maxCOS = (dim < 3 ? 2 : 4)};
    enum{maxBF = (dim < 3 ? 8 : 24)};
    enum{maxNFAP = (dim < 3 ? 4 : 8)};
    enum
    {
        leftIdx = 0,
        rightIdx = 1,
        bottomIdx
    };
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename Element::Geometry Geometry;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;

    typedef Dune::PQkLocalFiniteElementCache<CoordScalar, Scalar, dim, 1> LocalFiniteElementCache;
    typedef typename LocalFiniteElementCache::FiniteElementType LocalFiniteElement;
    typedef typename LocalFiniteElement::Traits::LocalBasisType::Traits LocalBasisTraits;
    typedef typename LocalBasisTraits::JacobianType ShapeJacobian;

    //dfm specific implementation
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;


public:
    /*!
     * \brief Constructor. Sets the initial element index.
     */
    BoxDFMFVElementGeoemtry()
    {
        curEIdxGlobal = -1;
    }

    void crossProduct(GlobalPosition& c, const GlobalPosition& a, const GlobalPosition& b) const
    {
        c[0] = a[1]*b[2] - a[2]*b[1];
        c[1] = a[2]*b[0] - a[0]*b[2];
        c[2] = a[0]*b[1] - a[1]*b[0];
    }

    void normalOfQuadrilateral3D(GlobalPosition &normal,
                                 const GlobalPosition& p0, const GlobalPosition& p1,
                                 const GlobalPosition& p2, const GlobalPosition& p3) const
    {
        GlobalPosition a = p2 - p0;
        GlobalPosition b = p3 - p1;
        crossProduct(normal, a, b);
        normal *= 0.5;
    }

    Scalar quadrilateralArea3D(const GlobalPosition& p0, const GlobalPosition& p1,
                               const GlobalPosition& p2, const GlobalPosition& p3) const
    {
        GlobalPosition normal;
        normalOfQuadrilateral3D(normal, p0, p1, p2, p3);
        return normal.two_norm();
    }

    void getFaceIndices(int numVertices, int scvfIdx, int& leftFace, int& rightFace) const
    {
        static const int edgeToFaceTet[2][6] = {
            {1, 0, 3, 2, 1, 3},
            {0, 2, 0, 1, 3, 2}
        };
        static const int edgeToFacePyramid[2][8] = {
            {0, 2, 3, 0, 1, 3, 4, 2},
            {1, 0, 0, 4, 3, 2, 1, 4}
        };
        static const int edgeToFacePrism[2][9] = {
            {1, 0, 2, 0, 3, 2, 4, 1, 4},
            {0, 2, 1, 3, 1, 3, 0, 4, 2}
        };
        static const int edgeToFaceHex[2][12] = {
            {0, 2, 3, 1, 4, 1, 2, 4, 0, 5, 5, 3},
            {2, 1, 0, 3, 0, 4, 4, 3, 5, 1, 2, 5}
        };

        switch (numVertices) {
        case 4:
            leftFace = edgeToFaceTet[0][scvfIdx];
            rightFace = edgeToFaceTet[1][scvfIdx];
            break;
        case 5:
            leftFace = edgeToFacePyramid[0][scvfIdx];
            rightFace = edgeToFacePyramid[1][scvfIdx];
            break;
        case 6:
            leftFace = edgeToFacePrism[0][scvfIdx];
            rightFace = edgeToFacePrism[1][scvfIdx];
            break;
        case 8:
            leftFace = edgeToFaceHex[0][scvfIdx];
            rightFace = edgeToFaceHex[1][scvfIdx];
            break;
        default:
            DUNE_THROW(Dune::NotImplemented,
                       "BoxFVElementGeometry :: getFaceIndices for numVertices = "
                       << numVertices);
            break;
        }
    }

    void getFaceAndEdgeIndicesAtVertex(int numVertices, int scvIdx, int& leftFace, int& rightFace, int& bottomFace,
            int& leftEdge, int& rightEdge, int& bottomEdge) const
    {
        static const int vertexToFaceTet[3][4] = {
            {2, 1, 3, 2},
            {1, 3, 2, 1},
            {0, 0, 0, 3}
        };

        static const int vertexToFaceHex[3][8] = {
            {0, 2, 3, 1, 0, 2, 3, 1},
            {2, 1, 0, 3, 2, 1, 0, 3},
            {4, 4, 4, 4, 5, 5, 5, 5}
        };

        static const int vertexToEdgeTet[3][4] = {
            {1, 0, 2, 5},
            {0, 2, 1, 4},
            {3, 4, 5, 3}
        };

        static const int vertexToEdgeHex[3][8] = {
            {4, 6, 7, 5, 8, 10, 11, 9},
            {6, 5, 4, 7, 10, 9, 8, 11},
            {0, 1, 2, 3, 0, 1, 2, 3}
        };

        switch (numVertices) {
        case 4:
            leftFace = vertexToFaceTet[leftIdx][scvIdx];
            rightFace = vertexToFaceTet[rightIdx][scvIdx];
            bottomFace = vertexToFaceTet[bottomIdx][scvIdx];
            leftEdge = vertexToEdgeTet[leftIdx][scvIdx];
            rightEdge = vertexToEdgeTet[rightIdx][scvIdx];
            bottomEdge = vertexToEdgeTet[bottomIdx][scvIdx];
            break;
        case 8:
            leftFace = vertexToFaceHex[leftIdx][scvIdx];
            rightFace = vertexToFaceHex[rightIdx][scvIdx];
            bottomFace = vertexToFaceHex[bottomIdx][scvIdx];
            leftEdge = vertexToEdgeHex[leftIdx][scvIdx];
            rightEdge = vertexToEdgeHex[rightIdx][scvIdx];
            bottomEdge = vertexToEdgeHex[bottomIdx][scvIdx];
            break;
        default:
            DUNE_THROW(Dune::NotImplemented, "BoxFractureFVElementGeometry :: getFaceIndices for numVertices = " << numVertices);
            break;
        }
    }

    struct FractureScvFace //! interior face of a fracture
    {
        Scalar area; //!< area of face
        Scalar fractureAreaFrac; //! fracture fraction of the scvf area
        Scalar matrixAreaFrac; //! matrix fraction of the scvf area
        Dune::FieldVector<Scalar,dim-1> faceScvfArea; //!< stores the areas of the faces connected to the scvf
        GlobalPosition normal; //!< normal vector of the face, has the magnitude of area
        int numFaceAtScvf; //!< the number of faces at a sub control volume face
        Dune::FieldVector<int,dim-1> faceIndices; //!< stores the indices of the faces connected to the scvf
        bool isFractureScvf; //!< does the scvf contain a fracture
    };

    struct FractureScv //! FV intersected with element
    {
        Scalar fractureVolFrac;
        Scalar matrixVolFrac;
        Scalar fractureVolume;
        Dune::FieldVector<Scalar,dim> faceScvVolume; //!< stores the volumes of the fracture faces connected to the scv
        Dune::FieldVector<int,dim> faceIndices; //!< stores the indices of the faces connected to the scvf
        Dune::FieldVector<int,dim> edgeIndices; //!< stores the indices of the faces connected to the scvf
        int numFaceAtScv; //!< the number of faces at a sub control volume
        bool isFractureScv; //!< this dof is connected to a fracture, not necessarily within this element
        bool hasFractureVolume; //!< this scv is connected to fracture faces within this element
    };

    //dof specific implementation
    mutable FractureScvFace fracScvFace[maxNE];//!< data of the fracture sub control volumes
    mutable FractureScv fracScv[maxNC]; //!< data of the fracture sub control volume faces
    mutable int curEIdxGlobal; //!< global index of the current element

    void updateFracture(const Element& element,
            const Problem& problem) const
    {
        int eIdxGlobal = problem.elementMapper().map(element);
        //Update fracture scv and scvf data if the element has changed
        if(curEIdxGlobal != eIdxGlobal)
        {
            updateFracture_(element, problem);
            curEIdxGlobal = eIdxGlobal;
        }
    }

private:

    void updateFracture_(const Element& element,
            const Problem& problem) const
    {
        //Get the reference element information
        const Geometry& geometry = element.geometry();
        const SpatialParams &spatialParams = problem.spatialParams();
        Dune::GeometryType geomType = geometry.type();
        const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
        int numScv = referenceElement.size(dim);
        int numScvf = referenceElement.size(dim-1);

        for(int scvIdx = 0; scvIdx < numScv; scvIdx++)
        {

            //initialize the volume of the fracture
            fracScv[scvIdx].fractureVolFrac = 0.;
            fracScv[scvIdx].matrixVolFrac = 1.;
            fracScv[scvIdx].fractureVolume = 0.;
            fracScv[scvIdx].faceScvVolume = 0.;
            fracScv[scvIdx].faceIndices = -1;
            fracScv[scvIdx].edgeIndices = -1;
            fracScv[scvIdx].numFaceAtScv = dim;
            fracScv[scvIdx].isFractureScv = false;
            fracScv[scvIdx].hasFractureVolume = false;

            if(spatialParams.isFractureVert(element, scvIdx))
            {
                fracScv[scvIdx].isFractureScv = true;
                if(dim == 2)
                {
                    int faceOrEdgeIdx = 0;
                    //Loop over the edges or intersections or sub control volume faces (all the same for dim = 2)
                    for (int scvfIdx=0; scvfIdx<referenceElement.size(/* codim = edges*/dim-1); scvfIdx++)
                    {
                        //opposing scvIdx i and j at scvf
                        int i = this->subContVolFace[scvfIdx].i;
                        int j = this->subContVolFace[scvfIdx].j;
                        //Check whether both dofs of the scvf are fracture dofs
                        if(i == scvIdx || j == scvIdx)
                        {
                            const GlobalPosition global_i = geometry.corner(i);
                            const GlobalPosition global_j = geometry.corner(j);
                            GlobalPosition diff_ij = global_j;
                            diff_ij -= global_i;
                            Scalar length = 0.5*diff_ij.two_norm();
                            Scalar fractureWidth = 0;
                            if(spatialParams.isFractureFace(element, scvfIdx))
                            {
                                fractureWidth = spatialParams.fractureWidth(element, *this, scvfIdx)/2;
                                fracScv[scvIdx].hasFractureVolume = true;
                            }
                            fracScv[scvIdx].faceIndices[faceOrEdgeIdx] = scvfIdx;
                            fracScv[scvIdx].faceScvVolume[faceOrEdgeIdx] = length*fractureWidth;
                            fracScv[scvIdx].fractureVolume += fracScv[scvIdx].faceScvVolume[faceOrEdgeIdx];
                            faceOrEdgeIdx++;
                        }
                    }
                    //faces and edges are the same in 2d
                    fracScv[scvIdx].edgeIndices = fracScv[scvIdx].faceIndices;
                    //Divide the fracture volume by the volume of the sub control volume to obtain the fracture volume fraction
                    fracScv[scvIdx].fractureVolFrac = fracScv[scvIdx].fractureVolume/this->subContVol[scvIdx].volume;

                    //if the scv volume is lesser than the fracture volume make the matrix volume fraction zero else 1-fractureVolFrac
                    if(Dune::FloatCmp::lt<Scalar>(this->subContVol[scvIdx].volume, fracScv[scvIdx].fractureVolume))
                        fracScv[scvIdx].matrixVolFrac = 0.;
                    else
                        fracScv[scvIdx].matrixVolFrac = 1 - fracScv[scvIdx].fractureVolFrac;
                }
                else if(dim == 3)
                {
                    //Get the indices of the faces and edges that contain the scv
                    getFaceAndEdgeIndicesAtVertex(numScv, scvIdx,
                            fracScv[scvIdx].faceIndices[leftIdx],
                            fracScv[scvIdx].faceIndices[rightIdx],
                            fracScv[scvIdx].faceIndices[bottomIdx],
                            fracScv[scvIdx].edgeIndices[leftIdx],
                            fracScv[scvIdx].edgeIndices[rightIdx],
                            fracScv[scvIdx].edgeIndices[bottomIdx]);

                    //Check if any face at the scv is a fracture face
                    if(spatialParams.isFractureFace(element, fracScv[scvIdx].faceIndices[leftIdx])
                            || spatialParams.isFractureFace(element, fracScv[scvIdx].faceIndices[rightIdx])
                            || spatialParams.isFractureFace(element, fracScv[scvIdx].faceIndices[bottomIdx]))
                    {
                        fracScv[scvIdx].hasFractureVolume = true;

                        //Coordinates of scv
                        GlobalPosition scvGlobal = geometry.corner(scvIdx);

                        //Declare the face centers in local and global coordinates
                        LocalPosition leftFaceCenterLocal;
                        LocalPosition rightFaceCenterLocal;
                        LocalPosition bottomFaceCenterLocal;
                        GlobalPosition leftFaceCenterGlobal;
                        GlobalPosition rightFaceCenterGlobal;
                        GlobalPosition bottomFaceCenterGlobal;

                        //Find the face centers in local and global coordinates
                        leftFaceCenterLocal = referenceElement.position(fracScv[scvIdx].faceIndices[leftIdx], /*codim faces*/ 1);
                        rightFaceCenterLocal = referenceElement.position(fracScv[scvIdx].faceIndices[rightIdx], /*codim faces*/ 1);
                        bottomFaceCenterLocal = referenceElement.position(fracScv[scvIdx].faceIndices[bottomIdx], /*codim faces*/ 1);
                        leftFaceCenterGlobal = geometry.global(leftFaceCenterLocal);
                        rightFaceCenterGlobal = geometry.global(rightFaceCenterLocal);
                        bottomFaceCenterGlobal = geometry.global(bottomFaceCenterLocal);

                        //Declare the face centers in local and global coordinates
                        LocalPosition leftEdgeCenterLocal;
                        LocalPosition rightEdgeCenterLocal;
                        LocalPosition bottomEdgeCenterLocal;
                        GlobalPosition leftEdgeCenterGlobal;
                        GlobalPosition rightEdgeCenterGlobal;
                        GlobalPosition bottomEdgeCenterGlobal;

                        //Find the edge centers in local and global coordinates
                        leftEdgeCenterLocal = referenceElement.position(fracScv[scvIdx].edgeIndices[leftIdx], /*codim edge*/ dim-1);
                        rightEdgeCenterLocal = referenceElement.position(fracScv[scvIdx].edgeIndices[rightIdx], /*codim edge*/ dim-1);
                        bottomEdgeCenterLocal = referenceElement.position(fracScv[scvIdx].edgeIndices[bottomIdx], /*codim edge*/ dim-1);
                        leftEdgeCenterGlobal = geometry.global(leftEdgeCenterLocal);
                        rightEdgeCenterGlobal = geometry.global(rightEdgeCenterLocal);
                        bottomEdgeCenterGlobal = geometry.global(bottomEdgeCenterLocal);

                        //calculatae the face volume within the scv
                        //divide fracture width by 2, as the fracture lives on both sides of the face
                        if(spatialParams.isFractureFace(element, fracScv[scvIdx].faceIndices[leftIdx]))
                        {
                            Scalar fractureWidthLeft = spatialParams.fractureWidth(element, *this, fracScv[scvIdx].faceIndices[leftIdx])/2; //[m]
                            fracScv[scvIdx].faceScvVolume[leftIdx] = this->quadrilateralArea3D(scvGlobal, leftEdgeCenterGlobal, leftFaceCenterGlobal, bottomEdgeCenterGlobal)
                                                *fractureWidthLeft; //[m2]
                        }

                        //calculatae the face volume within the scv
                        //divide fracture width by 2, as the fracture lives on both sides of the face
                        if(spatialParams.isFractureFace(element, fracScv[scvIdx].faceIndices[rightIdx]))
                        {
                            Scalar fractureWidthRight = spatialParams.fractureWidth(element, *this, fracScv[scvIdx].faceIndices[rightIdx])/2; //[m]
                            fracScv[scvIdx].faceScvVolume[rightIdx] = this->quadrilateralArea3D(scvGlobal, rightEdgeCenterGlobal, rightFaceCenterGlobal, bottomEdgeCenterGlobal)
                                                *fractureWidthRight; //[m2]
                        }

                        //calculatae the face volume within the scv
                        //divide fracture width by 2, as the fracture lives on both sides of the face
                        if(spatialParams.isFractureFace(element, fracScv[scvIdx].faceIndices[bottomIdx]))
                        {
                            Scalar fractureWidthBottom = spatialParams.fractureWidth(element, *this, fracScv[scvIdx].faceIndices[bottomIdx])/2; //[m]
                            fracScv[scvIdx].faceScvVolume[bottomIdx] = this->quadrilateralArea3D(scvGlobal, leftEdgeCenterGlobal, bottomFaceCenterGlobal, rightEdgeCenterGlobal)
                                                *fractureWidthBottom; //[m2]
                        }

                        //Calculate the total scv fracture volume
                        fracScv[scvIdx].fractureVolume = fracScv[scvIdx].faceScvVolume[leftIdx]
                                                                       + fracScv[scvIdx].faceScvVolume[rightIdx]
                                                                       + fracScv[scvIdx].faceScvVolume[bottomIdx];

                         //Divide the fracture volume by the volume of the sub control volume to obtain the fracture volume fraction
                        fracScv[scvIdx].fractureVolFrac = fracScv[scvIdx].fractureVolume/this->subContVol[scvIdx].volume;

                        //if the scv volume is lesser than the fracture volume make the matrix volume fraction zero else 1- fractureVolFrac
                        if(Dune::FloatCmp::lt<Scalar>(this->subContVol[scvIdx].volume, fracScv[scvIdx].fractureVolume))
                            fracScv[scvIdx].matrixVolFrac = 0.;
                        else
                            fracScv[scvIdx].matrixVolFrac = 1 - fracScv[scvIdx].fractureVolFrac;
                    }
                }
                else
                {
                    DUNE_THROW(Dune::NotImplemented, "BoxDFMFVElementGeometry :: wrong dimension. (Dim = " << dim << ")");
                }
            }
        }

        for (int scvfIdx = 0; scvfIdx < numScvf; scvfIdx++)
        {
            const Geometry& geometry = element.geometry();
            const SpatialParams &spatialParams = problem.spatialParams();

            //default fracture scvface values
            fracScvFace[scvfIdx].area = 0.0;
            fracScvFace[scvfIdx].fractureAreaFrac = 0.0;
            fracScvFace[scvfIdx].matrixAreaFrac = 1.0;
            fracScvFace[scvfIdx].faceScvfArea = 0.0;
            fracScvFace[scvfIdx].normal = 0;
            fracScvFace[scvfIdx].numFaceAtScvf = dim-1;
            fracScvFace[scvfIdx].faceIndices = -1;
            fracScvFace[scvfIdx].isFractureScvf = false;

            //Check whether both dofs are fracture dofs and that the face is no boundary face
            //necessary but not sufficient condition for a fracture in 2d and 3d
            if(spatialParams.isFractureVert(element, this->subContVolFace[scvfIdx].i) &&
                    spatialParams.isFractureVert(element, this->subContVolFace[scvfIdx].j))
            {
                //calculate the fracture face area
                if(dim == 2)
                {
                    //if the intersections is a fracture, the scvf must be a fracture scvf
                    if(spatialParams.isFractureFace(element, scvfIdx))
                    {
                        //Get the global position of the vertices
                        const GlobalPosition &globalPosI = geometry.corner(this->subContVolFace[scvfIdx].i);
                        const GlobalPosition &globalPosJ = geometry.corner(this->subContVolFace[scvfIdx].j);
                        Scalar fractureWidth;
                        fracScvFace[scvfIdx].isFractureScvf = true;
                        fracScvFace[scvfIdx].faceIndices[0] = scvfIdx;
                        //divide fracture width by 2, as the fracture lives on both sides of the interface
                        fractureWidth = spatialParams.fractureWidth(element, *this, scvfIdx)/2; //[m]
                        fracScvFace[scvfIdx].area = fractureWidth; //*1m [m2]
                        GlobalPosition normal = globalPosJ - globalPosI;
                        normal /= normal.two_norm();
                        fracScvFace[scvfIdx].normal = normal;
                        fracScvFace[scvfIdx].normal *= fracScvFace[scvfIdx].area;
                        fracScvFace[scvfIdx].faceScvfArea[0] = fracScvFace[scvfIdx].area;
                        fracScvFace[scvfIdx].fractureAreaFrac = fracScvFace[scvfIdx].area/this->subContVolFace[scvfIdx].area;
                        //if the scvf area is lesser than the fracture area make the matrix area fraction zero else 1- fractureVolFrac
                        if(Dune::FloatCmp::lt<Scalar>(this->subContVolFace[scvfIdx].area, fracScvFace[scvfIdx].area))
                            fracScvFace[scvfIdx].matrixAreaFrac = 0.;
                        else
                            fracScvFace[scvfIdx].matrixAreaFrac = 1 - fracScvFace[scvfIdx].fractureAreaFrac;
                    }
                    else
                        fracScvFace[scvfIdx].isFractureScvf = false;
                }
                else if(dim == 3)
                {
                    this->getFaceIndices(numScv, scvfIdx, fracScvFace[scvfIdx].faceIndices[leftIdx], fracScvFace[scvfIdx].faceIndices[rightIdx]);

                    //if one of the intersections is a fracture, the scvf must be a fracture scvf
                    if(spatialParams.isFractureFace(element, fracScvFace[scvfIdx].faceIndices[leftIdx]) ||
                            spatialParams.isFractureFace(element, fracScvFace[scvfIdx].faceIndices[rightIdx]))
                    {
                        fracScvFace[scvfIdx].isFractureScvf = true;
                        //Get the global position of the vertices
                        const GlobalPosition &globalPosI = geometry.corner(this->subContVolFace[scvfIdx].i);
                        const GlobalPosition &globalPosJ = geometry.corner(this->subContVolFace[scvfIdx].j);

                        //divide fracture width by 2, as the fracture lives on both sides of the interface
                        Scalar fractureWidthLeft;
                        if(spatialParams.isFractureFace(element, fracScvFace[scvfIdx].faceIndices[leftIdx]))
                            fractureWidthLeft = spatialParams.fractureWidth(element, *this, fracScvFace[scvfIdx].faceIndices[leftIdx])/2; //[m]
                        else
                            fractureWidthLeft = 0.;

                        Scalar fractureWidthRight;
                        if(spatialParams.isFractureFace(element, fracScvFace[scvfIdx].faceIndices[rightIdx]))
                            fractureWidthRight = spatialParams.fractureWidth(element, *this, fracScvFace[scvfIdx].faceIndices[rightIdx])/2; //[m]
                        else
                            fractureWidthRight = 0.;

                        //Find the interface centers in local and global coordinates
                        LocalPosition edgeCenterLocal = referenceElement.position(scvfIdx, /*codim faces*/ dim-1);
                        GlobalPosition edgeCenterGlobal = geometry.global(edgeCenterLocal);

                        LocalPosition leftFaceCenterLocal = referenceElement.position(fracScvFace[scvfIdx].faceIndices[leftIdx], /*codim faces*/ 1);
                        GlobalPosition leftFaceCenterGlobal = geometry.global(leftFaceCenterLocal);
                        GlobalPosition diff = edgeCenterGlobal - leftFaceCenterGlobal;
                        Scalar dist = diff.two_norm();
                        fracScvFace[scvfIdx].faceScvfArea[leftIdx] = dist*fractureWidthLeft;
                        fracScvFace[scvfIdx].area = fracScvFace[scvfIdx].faceScvfArea[leftIdx]; // [m2]

                        LocalPosition rightFaceCenterLocal = referenceElement.position(fracScvFace[scvfIdx].faceIndices[rightIdx], /*codim faces*/ 1);
                        GlobalPosition rightFaceCenterGlobal = geometry.global(rightFaceCenterLocal);
                        diff = edgeCenterGlobal - rightFaceCenterGlobal;
                        dist = diff.two_norm();
                        fracScvFace[scvfIdx].faceScvfArea[rightIdx] = dist*fractureWidthRight;
                        fracScvFace[scvfIdx].area += fracScvFace[scvfIdx].faceScvfArea[rightIdx]; // [m2]

                        //Calculate normal, the normal is always parallel to the connecting vector between the two dofs
                        //and has the magnitude of the calculated area
                        GlobalPosition normal = globalPosJ - globalPosI;
                        normal /= normal.two_norm();
                        fracScvFace[scvfIdx].normal = normal;
                        fracScvFace[scvfIdx].normal *= fracScvFace[scvfIdx].area;
                        fracScvFace[scvfIdx].fractureAreaFrac = fracScvFace[scvfIdx].area/this->subContVolFace[scvfIdx].area;
                        //if the scvf area is lesser than the fracture area make the matrix area fraction zero else 1- fractureVolFrac
                        if(Dune::FloatCmp::lt<Scalar>(this->subContVolFace[scvfIdx].area, fracScvFace[scvfIdx].area))
                            fracScvFace[scvfIdx].matrixAreaFrac = 0.;
                        else
                            fracScvFace[scvfIdx].matrixAreaFrac = 1 - fracScvFace[scvfIdx].fractureAreaFrac;
                    }
                }
                else
                {
                    DUNE_THROW(Dune::NotImplemented, "BoxDFMFVElementGeometry :: wrong dimension. (Dim = " << dim << ")");
                }
            }
        }
    }
};
}

#endif

