// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup OnePTwoCModel
 * \file
 *
 * \brief Defines the properties required for the single-phase,
 *        two-component fully implicit model.
 */

#ifndef DUMUX_1P2CDFM_PROPERTIES_HH
#define DUMUX_1P2CDFM_PROPERTIES_HH

#include <dumux/implicit/1p2c/1p2cproperties.hh>
#include <dumux/implicit/box/boxproperties.hh>
#include <dumux/implicit/cellcentered/ccproperties.hh>
#include <dumux/implicit/dfm/common/dfmproperties.hh>

namespace Dumux
{
// \{
namespace Properties
{

//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tags for the corresponding dfm problems
NEW_TYPE_TAG(OnePTwoCDFM, INHERITS_FROM(OnePTwoC, DiscreteFractureModel));
NEW_TYPE_TAG(BoxOnePTwoCDFM, INHERITS_FROM(BoxModel, OnePTwoCDFM));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
NEW_PROP_TAG(ProblemTransportEqMatrixWeight);
NEW_PROP_TAG(ProblemTransportEqFractureWeight);

}
// \}
}

#endif

