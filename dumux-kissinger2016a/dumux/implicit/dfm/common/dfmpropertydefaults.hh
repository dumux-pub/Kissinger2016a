// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup DFMModel
 * \file
 *
 * \brief Defines default values for most properties required by the
 *        implicit DiscreteFractureModel.
 */
#ifndef DUMUX_DFM_PROPERTY_DEFAULTS_HH
#define DUMUX_DFM_PROPERTY_DEFAULTS_HH

#include "dfmvolumevariables.hh"
#include "dfmfluxvariables.hh"
#include <dumux/implicit/dfm/boxdfm/boxdfmfvelementgeometry.hh>
#include <dumux/implicit/box/boxproperties.hh>

namespace Dumux
{

namespace Properties
{

//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

SET_TYPE_PROP(DiscreteFractureModel, VolumeVariables, DFMVolumeVariables<TypeTag>);

SET_TYPE_PROP(DiscreteFractureModel, FluxVariables, DFMFluxVariables<TypeTag>);

SET_TYPE_PROP(DiscreteFractureModel, FVElementGeometry, BoxDFMFVElementGeoemtry<TypeTag>);

}

}
#endif
