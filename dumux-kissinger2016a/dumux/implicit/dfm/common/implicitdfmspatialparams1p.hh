// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters of single-phase fracture problems
 * using a fully implicit discretization method.
 */
#ifndef DUMUX_IMPLICIT_DFM_SPATIAL_PARAMS_ONEP_HH
#define DUMUX_IMPLICIT_DFM_SPATIAL_PARAMS_ONEP_HH

#include <dune/common/float_cmp.hh>
#include <dune/geometry/referenceelements.hh>
#include <dumux/material/spatialparams/implicitspatialparams1p.hh>

namespace Dumux {
// forward declaration of property tags
namespace Properties {

}

/*!
 * \ingroup SpatialParameters
 */


/**
 * \brief The base class for spatial parameters of single-phase fracture problems
 * using a fully implicit discretization method.
 */
template<class TypeTag>
class ImplicitDFMSpatialParamsOneP: public ImplicitSpatialParamsOneP<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView:: template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    template<int dim>
    struct FaceLayout
    {
        bool contains (Dune::GeometryType geomType)
        {
            return geomType.dim() == dim - 1;
        }
    };

    enum {
        dimWorld = GridView::dimensionworld,
        dim = GridView::dimension
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::FractureScv FractureScv;
    typedef typename FVElementGeometry::FractureScvFace FractureScvFace;
    typedef typename GridView::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGVertexLayout> VertexMapper;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, FaceLayout> FaceMapper;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

public:
    ImplicitDFMSpatialParamsOneP(const GridView &gridView, const Problem &problem)
    :ImplicitSpatialParamsOneP<TypeTag>(gridView), gridView_(gridView), faceMapper_(gridView), problem_(problem)
    {
        //resize and initialize the fracture vectors with false
        int numFaces = gridView.size(/*codim faces*/ 1);
        fractureFace_.resize(numFaces, false);
        int numVertices = gridView.size(/*codim faces*/ dim);
        fractureVert_.resize(numVertices, false);
    }

    /*!
     * \brief Function for defining the intrinsic (absolute) fracture permeability.
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param fIdx The index of the face.
     * \return the intrinsic fracture permeability
     */
    Scalar fracturePermeability(const Element &element,
            const FVElementGeometry &fvGeometry,
            const int fIdx) const
    {
        Dune::GeometryType geomType = element.geometry().type();
        const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
        GlobalPosition globalPos = element.geometry().global(referenceElement.position(fIdx, /*codim faces*/ 1));
        return asImp_().fracturePermeabilityAtPos(element.geometry().center());
    }

    /*!
     * \brief Function for defining the intrinsic (absolute) fracture permeability.
     *
     * \return intrinsic (absolute) permeability
     * \param globalPos The position of the center of the face
     */
    Scalar fracturePermeabilityAtPos (const GlobalPosition& globalPos) const
    {
        DUNE_THROW(Dune::InvalidStateException,
                   "The spatial parameters do not provide "
                   "a fracturePermeabilityAtPos() method.");
    }

    /*!
     * \brief Function for defining the fracture porosity.
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param fIdx The index of the face.
     * \return the fracture porosity
     */
    Scalar fracturePorosity(const Element &element,
            const FVElementGeometry &fvGeometry,
            const int fIdx) const
    {
        Dune::GeometryType geomType = element.geometry().type();
        const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
        GlobalPosition globalPos = element.geometry().global(referenceElement.position(fIdx, /*codim faces*/ 1));
        return asImp_().fracturePorosityAtPos(globalPos);
    }

    /*!
     * \brief Function for defining the fracture porosity.
     *
     * \return porosity
     * \param globalPos The position of the center of the face
     */
    Scalar fracturePorosityAtPos(const GlobalPosition& globalPos) const
    {
        DUNE_THROW(Dune::InvalidStateException,
                   "The spatial parameters do not provide "
                   "a fracturePorosityAtPos() method.");
    }

    /*!
     * \brief Function for defining the fracture width.
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param fIdx The index of the face.
     * \return the fracture width
     */
    Scalar fractureWidth(const Element &element,
            const FVElementGeometry &fvGeometry,
            const int fIdx) const
    {
        Dune::GeometryType geomType = element.geometry().type();
        const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
        GlobalPosition globalPos = element.geometry().global(referenceElement.position(fIdx, /*codim faces*/ 1));
        return asImp_().fracturePorosityAtPos(element.geometry().center());
    }

    /*!
     * \brief Function for defining the fracture width.
     *
     * \return porosity
     * \param globalPos The position of the center of the face
     */
    Scalar fractureWidthAtPos(const GlobalPosition& globalPos) const
    {
        DUNE_THROW(Dune::InvalidStateException,
                   "The spatial parameters do not provide "
                   "a fractureWidthAtPos() method.");
    }

    /*!
     * \brief Function for calculating the volume averaged fracture porosity in an scv.
     *
     * \param element The current element
     * \param fracScv The current geometry of the fractures at the scv
     * \return the fracture porosity
     */
    Scalar meanFracturePorosity(const Element &element,
            const FVElementGeometry &fvGeometry,
            const FractureScv &fracScv) const
    {
        //Check if the scv has a fracture volume
        if(fracScv.hasFractureVolume)
        {
            Scalar fracturePor = 0.;
            //Check if the fracture Scv total volume is zero
            if(Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::CmpStyle::absolute>(0, fracScv.fractureVolume, 1e-10))
            {
                DUNE_THROW(Dune::InvalidStateException,
                        "Fracture scv volume is too small (division by zero): "<<fracScv.fractureVolume <<"");
            }
            //Loop over faces at scv and calulate the volume averaged porosity of the fracture
            for(int faceIdx = 0; faceIdx < fracScv.numFaceAtScv; faceIdx++)
            {
                Scalar facePor = 0.;
                //Get the porosity of the interface connected to the fracture scvf
                if(isFractureFace(element, fracScv.faceIndices[faceIdx]))
                       facePor = asImp_().fracturePorosity(element, fvGeometry, fracScv.faceIndices[faceIdx]);

                fracturePor += (fracScv.faceScvVolume[faceIdx]*facePor)/fracScv.fractureVolume;
            }
            return fracturePor;
        }
        else
            return 0.;
    }

    /*!
     * \brief Function for calculating the area averaged fracture permeability in an scvf.
     *
     * \param element The current element
     * \param fracScvF The current finite volume geometry of the fractures at the scvf
     * \return the fracture permeability
     */
    Scalar meanFractureK(const Element &element,
            const FVElementGeometry &fvGeometry,
            const FractureScvFace &fracScvf) const
    {
        //Calculate the average permeability over the fracture face using a weighted arithmetic average
        //Check whether the fracScvF_.area is larger than zero using DUNE::FLOATCOMPARE with an absolute criterion of 1e-10 sm
        //Check if the scv has a fracture volume
        if(fracScvf.isFractureScvf)
        {
            Scalar fractureK = 0.;
            if(Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::CmpStyle::absolute>(0, fracScvf.area, 1e-10))
            {
                DUNE_THROW(Dune::InvalidStateException,
                                       "Fracture scvf area is too small (division by zero): "<<fracScvf.area <<"");
            }
            for(int faceIdx = 0; faceIdx < fracScvf.numFaceAtScvf; faceIdx++)
            {
                Scalar faceK = 0;
                //Get the permeability of the interface connected to the fracture scvf
                if(isFractureFace(element, fracScvf.faceIndices[faceIdx]))
                        faceK = asImp_().fracturePermeability(element, fvGeometry, fracScvf.faceIndices[faceIdx]);

                fractureK += (fracScvf.faceScvfArea[faceIdx]*faceK)/fracScvf.area;
            }
            return fractureK;
        }
        else
            return 0.;
    }

    /*!
     * \brief Checks whether element intersection is a fracture.
     *
     * \param element The current element
     * \param fIdx Face index to be checked
     */
    bool isFractureFace(const Element &element,
            const int fIdx) const
    {
        int fIdxGlobal = faceMapper_.map(element, fIdx, /*codim faces*/1);
        return fractureFace_[fIdxGlobal];
    }

    /*!
     * \brief Set a fracture on an element face and set the fracture vertices of the face as well.
     *
     * \param element The current element
     * \param fIdx Face index to be checked
     * \param value Bool value to be assigned to face
     */
    void setFractureFace(const Element &element,
            const int fIdx, bool value)
    {
        int fIdxGlobal = faceMapper_.map(element, fIdx, /*codim faces*/1);
        fractureFace_[fIdxGlobal] = value;

        //set the fracture vertices on the face
        Dune::GeometryType geomType =element.geometry().type();
        const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
        //Loop over vertices in fracture face
        int numVerticesOfFace = referenceElement.size(fIdx, 1, dim);
        for (int vIdxInFace = 0; vIdxInFace < numVerticesOfFace; vIdxInFace++)
        {
            int vIdx = referenceElement.subEntity(fIdx, /*codim face*/1, vIdxInFace, /*codim vertex*/dim);
            setFractureVert(element, vIdx, true);
        }
    }

    /*!
     * \brief Checks whether vertex is a fracture.
     *
     * \param element The current element
     * \param localVertexIdx local vertex index with respect to the element
     */
    bool isFractureVert(const Element &element,
            const int localVertIdx) const
    {
        int vIdxGlobal = problem().vertexMapper().map(element, localVertIdx, /*codim vertex*/dim);
        return fractureVert_[vIdxGlobal];
    }

    /*!
     * \brief Checks whether vertex is a fracture.
     *
     * \param vIdxGlobal global vertex index with respect to the element
     */
    bool isFractureVert(const int vIdxGlobal) const
    {
        return fractureVert_[vIdxGlobal];
    }

    /*!
     * \brief A reference to the problem on which the model is applied.
     */
    const Problem &problem() const
    { return problem_; }

    /*!
     * \brief A reference to the face mapper.
     */
    const FaceMapper &faceMapper() const
    { return faceMapper_; }

private:

    /*!
     * \brief Set a fracture vertex.
     *
     * \param element The current element
     * \param localVertexIdx Vertex index to be checked
     * \param value Bool value to be assigned to vertex
     */
    void setFractureVert(const Element &element,
            const int localVertIdx, const bool value)
    {
        int vIdxGlobal = problem().vertexMapper().map(element, localVertIdx, /*codim vertex*/dim);
        fractureVert_[vIdxGlobal] = value;
    }

    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

    std::vector<bool> fractureFace_;
    std::vector<bool> fractureVert_;
    const GridView& gridView_;
    FaceMapper faceMapper_;
    const Problem& problem_;

};

} // namespace Dumux

#endif
