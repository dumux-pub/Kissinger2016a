// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        all fluxes of fluid phases over a face of a finite volume.
 *
 * This means pressure and mole-fraction gradients, phase densities at
 * the integration point, etc.
 *
 */
#ifndef DUMUX_DFM_FLUX_VARIABLES_HH
#define DUMUX_DFM_FLUX_VARIABLES_HH

#include <dune/common/float_cmp.hh>

#include <dumux/common/math.hh>
#include <dumux/common/valgrind.hh>

#include "dfmproperties.hh"


namespace Dumux
{

namespace Properties
{
// forward declaration of properties
NEW_PROP_TAG(ImplicitMobilityUpwindWeight);
NEW_PROP_TAG(SpatialParams);
NEW_PROP_TAG(NumPhases);
NEW_PROP_TAG(ProblemEnableGravity);
}

/*!
 * \ingroup OnePTwoCDFModel
 * \ingroup ImplicitFluxVariables
 * \brief This template class contains the data which is required to
 *        calculate the fluxes of the fluid phases over a face of a
 *        finite volume for the one-phase, two-component model.
 *
 * This means pressure and mole-fraction gradients, phase densities at
 * the intergration point, etc.
 */
template <class TypeTag>
class DFMFluxVariables : public GET_PROP_TYPE(TypeTag, MatrixFluxVariables)
{
    typedef typename GET_PROP_TYPE(TypeTag, MatrixFluxVariables) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld

    };
    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases)} ;

    typedef typename GridView::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::SubControlVolumeFace ScvFace;

    //dfm specific implementation
    //define a struct similar FVElementGeometry::SubControlVolumeFace for the fracture
    typedef typename FVElementGeometry::FractureScvFace FractureScvFace;

public:
    /*
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the fully implicit scheme
     * \param scvfIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    DFMFluxVariables(const Problem &problem,
                          const Element &element,
                          const FVElementGeometry &fvGeometry,
                          const int faceIdx,
                          const ElementVolumeVariables &elemVolVars,
                          const bool onBoundary = false)
        : ParentType(problem, element, fvGeometry, faceIdx, elemVolVars, onBoundary), fvGeometry_(fvGeometry), faceIdx_(faceIdx)
    {
        //dfm specific implementation is fracture
        mobilityUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MobilityUpwindWeight);

        //initialize
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            fractureVolumeFlux_[phaseIdx] = Scalar(0);
            fractureVelocity_[phaseIdx] = Scalar(0);
            fractureKGradPNormal_[phaseIdx] = Scalar(0);
            fractureKGradP_[phaseIdx] = Scalar(0);
            fracturePotentialGrad_[phaseIdx] = Scalar(0);
            upstreamFractureIdx_[phaseIdx] = ParentType::upstreamIdx(phaseIdx);
            downstreamFractureIdx_[phaseIdx] = ParentType::downstreamIdx(phaseIdx);
        }
        //Check if this is an actual fracture face and make sure that it is not a boundary face
        //then perform the below operations
        if(fractureFace().isFractureScvf && !onBoundary)
        {
            calculateGradients_(problem, element, elemVolVars);
            calculateNormalVelocity_(problem, element, elemVolVars);
        }

    };

public:
    /*!
     * \brief Return intrinsic permeability multiplied with potential
     *        gradient multiplied with normal.
     *        I.e. everything that does not need upwind decisions.
     *
     * \param phaseIdx index of the phase
     */
   Scalar fractureKGradPNormal(const unsigned int phaseIdx) const
   { return fractureKGradPNormal_[phaseIdx]; }

   /*!
    * \brief Return the pressure potential multiplied with the
    *        intrinsic permeability as vector (for velocity output).
    */
   GlobalPosition fractureKGradP(const unsigned int phaseIdx) const
   { return fractureKGradP_[phaseIdx]; }

    /*!
     * \brief Return the pressure potential gradient \f$\mathrm{[Pa/m]}\f$.
     */
    const GlobalPosition &fracturePotentialGrad(const unsigned int phaseIdx) const
    { return fracturePotentialGrad_[phaseIdx]; }

    /*!
     * \brief Return the volumetric flux over a face of a given phase.
     *
     *        This is the calculated velocity multiplied by the unit normal
     *        and the area of the face.
     *        face().normal
     *        has already the magnitude of the area.
     *
     * \param phaseIdx index of the phase
     */
//    Scalar volumeFlux(const unsigned int phaseIdx) const
//    {
//        assert (phaseIdx == Indices::phaseIdx);
//        //Get the matrix volume flux and add the fracture volume flux
//        Scalar totalVolumeFlux = ParentType::volumeFlux(phaseIdx);
//        totalVolumeFlux += fractureVolumeFlux_[phaseIdx];
//        return totalVolumeFlux;
//    }

    /*!
     * \brief Return the volumetric flux over a face of a given phase.
     *
     *        This is the calculated velocity multiplied by the unit normal
     *        and the area of the face.
     *        face().normal
     *        has already the magnitude of the area.
     *
     * \param phaseIdx index of the phase
     */
    Scalar fractureVolumeFlux(const unsigned int phaseIdx) const
    {
        //Get the fracture volume flux
        return fractureVolumeFlux_[phaseIdx];
    }

    /*!
     * \brief Return the local index of the downstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int downstreamFractureIdx(const unsigned phaseIdx) const
    { return downstreamFractureIdx_[phaseIdx]; }

    /*!
     * \brief Return the local index of the upstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int upstreamFractureIdx(const unsigned phaseIdx) const
    { return upstreamFractureIdx_[phaseIdx]; }

    /*!
     * \brief Return whether the geometry of the fracture scvface.
     *
     */
    const FractureScvFace &fractureFace() const
    {
        return fvGeometry_.fracScvFace[faceIdx_];
    }

protected:

    /*!
     * \brief Calculation of the pressure gradients within a fracture.
     *
     *        \param problem The considered problem file
     *        \param element The considered element of the grid
     *        \param elemVolVars The parameters stored in the considered element
     */
    void calculateGradients_(const Problem &problem,
                             const Element &element,
                             const ElementVolumeVariables &elemVolVars)
    {

        // loop over all phases
        for (int phaseIdx = 0; phaseIdx < numPhases; phaseIdx++)
        {
            GlobalPosition tmp;

            // dfm specific implementation uses two-point gradients with modified geometry at fractures
            const GlobalPosition &globalPosI = element.geometry().corner(this->face().i);
            const GlobalPosition &globalPosJ = element.geometry().corner(this->face().j);
            tmp = globalPosI;
            tmp -= globalPosJ;
            tmp /= tmp.two_norm2();

            fracturePotentialGrad_[phaseIdx] = tmp;
            fracturePotentialGrad_[phaseIdx] *= elemVolVars[this->face().i].fluidState().pressure(phaseIdx)
                                            - elemVolVars[this->face().j].fluidState().pressure(phaseIdx);

            // correct the pressure gradient by the gravitational acceleration
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
            {
                // ask for the gravitational acceleration at the given SCV face
                GlobalPosition g(problem.gravityAtPos(this->face().ipGlobal));

                // calculate the phase density at the integration point. we
                // only do this if the wetting phase is present in both cells
                Scalar SI = elemVolVars[this->face().i].fluidState().saturation(phaseIdx);
                Scalar SJ = elemVolVars[this->face().j].fluidState().saturation(phaseIdx);
                Scalar rhoI = elemVolVars[this->face().i].fluidState().density(phaseIdx);
                Scalar rhoJ = elemVolVars[this->face().j].fluidState().density(phaseIdx);
                Scalar fI = std::max(0.0, std::min(SI/1e-5, 0.5));
                Scalar fJ = std::max(0.0, std::min(SJ/1e-5, 0.5));
                if (Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(fI + fJ, 0.0, 1.0e-30))
                    // doesn't matter because no wetting phase is present in
                    // both cells!
                    fI = fJ = 0.5;
                const Scalar density = (fI*rhoI + fJ*rhoJ)/(fI + fJ);

                // make gravity acceleration a force
                GlobalPosition f(g);
                f *= density;

                // calculate the final potential gradient
                fracturePotentialGrad_[phaseIdx] -= f;
            } // gravity
        } // loop over all phases
    }

    /*
     * \brief Actual calculation of the normal Darcy velocities.
     *
     * \param problem The problem
     * \param element The finite element
     * \param elemVolVars The volume variables of the current element
     */
    void calculateNormalVelocity_(const Problem &problem,
            const Element &element,
            const ElementVolumeVariables &elemVolVars)
    {
        // calculate the mean intrinsic permeability
        const SpatialParams &spatialParams = problem.spatialParams();
        Scalar K = spatialParams.meanFractureK(element, fvGeometry_, fractureFace()); //fracture permeability assigned to the interface
        // loop over all phases
        for (int phaseIdx = 0; phaseIdx < numPhases; phaseIdx++)
        {
            // calculate the flux in the normal direction of the
            // current sub control volume face:
            //
            // v = - (K_f grad phi) * n
            // with K_f = rho g / mu K
            //
            // Mind, that the normal has the length of it's area.
            // This means that we are actually calculating
            //  Q = - (K grad phi) dot n /|n| * A

            fractureKGradP_[phaseIdx] = fracturePotentialGrad_[phaseIdx];
            fractureKGradP_[phaseIdx] *= K;
            fractureKGradPNormal_[phaseIdx] = fractureKGradP_[phaseIdx]*fractureFace().normal;

//            upstreamFractureIdx_[phaseIdx] = this->upstreamIdx(phaseIdx);
//            downstreamFractureIdx_[phaseIdx] = this->downstreamIdx(phaseIdx);
            // determine the upwind direction of the fracture
            if (fractureKGradPNormal_[phaseIdx] < 0)
            {
                upstreamFractureIdx_[phaseIdx] = this->face().i;
                downstreamFractureIdx_[phaseIdx] = this->face().j;
            }
            else
            {
                upstreamFractureIdx_[phaseIdx] = this->face().j;
                downstreamFractureIdx_[phaseIdx] = this->face().i;
            }
//            if(upstreamFractureIdx_[phaseIdx] != this->upstreamIdx(phaseIdx))
//            {
//                std::cout<<"Different upstream nodes for matrix and fracture!"<<std::endl;
//            }
//            else
//                std::cout<<"Same upstream nodes for matrix and fracture!"<<std::endl;
            // obtain the upwind volume variables
            const VolumeVariables& upVolVars = elemVolVars[ upstreamFractureIdx(phaseIdx) ];
            const VolumeVariables& downVolVars = elemVolVars[ downstreamFractureIdx(phaseIdx) ];

            // the minus comes from the Darcy relation which states that
            // the flux is from high to low potentials.
            // set the velocity
            fractureVelocity_[phaseIdx] = fractureKGradP_[phaseIdx];
            fractureVelocity_[phaseIdx] *= - ( mobilityUpwindWeight_*upVolVars.fractureMobility(phaseIdx)
                    + (1.0 - mobilityUpwindWeight_)*downVolVars.fractureMobility(phaseIdx)) ;

            // set the volume flux
            fractureVolumeFlux_[phaseIdx] = fractureVelocity_[phaseIdx] * fractureFace().normal ;
        }// loop all phases
    }

    Scalar          mobilityUpwindWeight_; //!< Upwind weight for mobility. Set to one for full upstream weighting
    Scalar          fractureVolumeFlux_[numPhases] ;    //!< Velocity multiplied with normal (magnitude=area)
    GlobalPosition  fractureVelocity_[numPhases] ;      //!< The velocity as determined by Darcy's law or by the Forchheimer relation
    Scalar          fractureKGradPNormal_[numPhases] ;  //!< Permeability multiplied with gradient in potential, multiplied with normal (magnitude=area)
    GlobalPosition  fractureKGradP_[numPhases] ; //!< Permeability multiplied with gradient in potential
    GlobalPosition  fracturePotentialGrad_[numPhases] ; //!< Gradient of potential, which drives flow
    const FVElementGeometry &fvGeometry_;
    unsigned int upstreamFractureIdx_[numPhases], downstreamFractureIdx_[numPhases]; //!< local index of the upstream / downstream fracture vertex
    const unsigned int faceIdx_; //!< local index of the sub control volume face
};

} // end namespace

#endif
