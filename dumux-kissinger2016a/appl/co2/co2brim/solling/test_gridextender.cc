/**
 * @file grdecl2vtu.C
 * @brief Converts grdecl (Eclipse grid) files to vtu (VTK/ParaView)
 * 
 * Converts a corner-point grid with properties to a vtu-file
 * (to be opened in ParaView for example)
 *
 * Based on make_vtk_test.cpp
 *
 * @author H�vard Berland <havb@statoil.com>
 * @author Atgeirr F Rasmussen <atgeirr@sintef.no>
 * @author B�rd Skaflestad     <bard.skaflestad@sintef.no>
 *
 * $Id: grdecl2vtu.C 499 2010-05-12 06:37:46Z havb $
 *
 */



#include "config.h"
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/dgf.hh>
#endif

#include <iostream>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/grid/io/file/dgfparser.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dumux/common/basicproperties.hh>
#include <dumux/common/start.hh>
#include "helperclasses/dgfextender.hh"

using namespace Dune;

namespace Dumux
{

namespace Properties
{
NEW_TYPE_TAG(GridExtenderTest, INHERITS_FROM(NumericModel));
NEW_PROP_TAG(VtkWriteVtk);
//Set the grid type
SET_TYPE_PROP(GridExtenderTest, Grid, Dune::ALUGrid<3,3,Dune::cube,Dune::conforming>);
//Set the grid extender type
SET_TYPE_PROP(GridExtenderTest, DGFExtender, Dumux::DGFExtender<TypeTag>);
//
SET_BOOL_PROP(GridExtenderTest, VtkWriteVtk, false);
}
}

int main(int argc, char** argv)
{
    // this method calls MPI_Init, if MPI is enabled
    Dune::MPIHelper &mpihelper = Dune::MPIHelper::instance( argc, argv );

    typedef typename TTAG(GridExtenderTest) TypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef typename GET_PROP_TYPE(TypeTag, DGFExtender) DGFExtender;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;

    if (argc != 2) {
        std::cout << "Usage: ./test_gridextender PARAMETER-FILE.input" << std::endl;
        exit(1);
    }
    std::string fileName = argv[1];
    // Read the parameters from the input file
    Dune::ParameterTreeParser::readINITree(fileName, ParameterTree::tree());

    std::string dgfFileName = GET_RUNTIME_PARAM(TypeTag, std::string, Grid.File);
    std::string fname(dgfFileName);
    std::string fnamebase = fname.substr(0, fname.find_last_of('.'));
    std::string dgfFileNameExtend = fnamebase + "_extend.dgf";
    //Read grid from dgf-file and write extented grid
    //brackets are used so that the original grid gets destructed again
    //before vtk output is created from the new grid
    {
        Dune::GridPtr<Grid> gridPtr(dgfFileName.c_str()/*, Dune::MPIHelper::getCommunicator()*/);
        DGFExtender dgfWriter(gridPtr->leafGridView(), gridPtr);
        try
        {
            dgfWriter.write(dgfFileNameExtend);
        }
        catch(Dumux::ParameterException &e)
        {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch(Dune::Exception &e)
        {
            std::cerr << "Dune reported error: " << e << std::endl;
            exit(1) ;
        }
        catch(...)
        {
            std::cerr << "Unknown exception thrown while reading in parameters in the problem file!\n";
            exit(1);
        }
    }
    //Write vtk file of the extended grid
    if (GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, WriteVtk))
    {
        std::cout<<"Writing vtk file"<<std::endl;
        //Make the grid
        Dune::GridPtr<Grid> gridPtr(dgfFileNameExtend.c_str()/*, Dune::MPIHelper::getCommunicator()*/);
        // construct a vtk output writer and attach the boundaryMakers
        Dune::VTKSequenceWriter<Grid::LeafGridView> vtkWriter(gridPtr->leafGridView(), "extend", ".", "");
        vtkWriter.write(0);
    }
    std::cout<<"success!"<<std::endl;
    return 0;
}



