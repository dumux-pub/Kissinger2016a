// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outlfow problem.
 */
#ifndef DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH
#define DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH

#if MODELTYPE == 1
#include <dumux/implicit/dfm/common/implicitdfmspatialparams1p.hh>
#include <dumux/io/dgfgridcreator.hh>
#else
#include <dumux/implicit/dfm/common/implicitdfmspatialparams.hh>
#endif
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include "../helperclasses/vertidxtoelemneighbormapper.hh"

#include <dune/common/float_cmp.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class SollingSpatialParams;

namespace Properties
{

#if MODELTYPE != 1
// The spatial parameters TypeTag
NEW_TYPE_TAG(SollingSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(SollingSpatialParams, SpatialParams, Dumux::SollingSpatialParams<TypeTag>);
// Set the material Law
SET_PROP(SollingSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};
#endif
}
/*!
 * \ingroup OnePTwoCBoxModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outflow problem.
 */
template<class TypeTag>
#if MODELTYPE == 1
class SollingSpatialParams : public ImplicitDFMSpatialParamsOneP<TypeTag>
#elif MODELTYPE == 2
class SollingSpatialParams : public ImplicitDFMSpatialParams<TypeTag>
#else
class SollingSpatialParams : public FVSpatialParams<TypeTag>
#endif
{
    // Parent Type
#if MODELTYPE == 1
    typedef ImplicitDFMSpatialParamsOneP<TypeTag> ParentType;
#elif MODELTYPE == 2
    typedef ImplicitDFMSpatialParams<TypeTag> ParentType;
#else
    typedef FVSpatialParams<TypeTag> ParentType;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

    typedef typename GridView:: template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        pressureIdx = Indices::pressureIdx,
    };
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

    //Vectors storing permeability and porosity values at every element for faster access
    typedef std::vector<Scalar> PermeabilityType;
    typedef std::vector<Scalar> PorosityType;
  //  typedef Dune::ReservoirPropertyCapillary<3> ReservoirProperties;
    typedef typename GET_PROP(TypeTag, ParameterTree) Params;

#if MODELTYPE != 1
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef Dumux::VertIdxToElemNeighborMapper<GridView> VertIdxToElemNeighborMapper;
    typedef typename VertIdxToElemNeighborMapper::NeighborElementSeedsIterator NeighborElementSeedsIterator;
    //typedef LinearMaterial<Scalar> EffMaterialLaw;
public:

    enum {
        //Layer indices
        quarIdx=0,
        terIdx=1,
        rcIdx=2,
        oliIdx=3,
        obsIdx=4,
        mbsIdx=5,
        solIdx=6,
        umbsIdx=7,
        ubsIdx=8,
        zecIdx=9,
        kreideIdx=10,
        tzIdx=11,
        quarTopIdx = 12,

        //Flux line indices
        noFluxIdx=0,
        rupelFluxIdx=1,
    };

    enum {
        //indices of element parameters specified in dgf file
        fluxElemIdx = 0,
        layerEIdx = 1,
        riverElemIdx = 2,
        elemRowIdx = 3,
        outerDomainEIdx = 4
    };

    enum {
        //indices of element markers to be used with elementMarkerMap_
        eIdxTZ = 0,
        eIdxRiver = 1,

        vIdxSB = 1,
        vIdxTerQuar = 2,
        vIdxRiver = 3,
    };

    SollingSpatialParams(const GridView &gridView, const Problem &problem)
        : ParentType(gridView, problem), gridView_(gridView), vertexElementMapper_(gridView)
    {
        try
        {
            Swr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Swr);
            Snr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Snr);
            brooksCoreyPe_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyPe);
            brooksCoreyLambda_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyLambda);
            compressibility_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Compressibility);
            longDispersion_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LongDispersion);
            transDispersion_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.TransDispersion);
            initialPermeability_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.InitialPermeability);
            arrayPerm_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermQuar);
            arrayPerm_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermTer);
            arrayPerm_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermRC);
            arrayPerm_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermOli);
            arrayPerm_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermOBS);
            arrayPerm_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermMBS);
            arrayPerm_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermSol);
            arrayPerm_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermUMBS);
            arrayPerm_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermUBS);
            arrayPerm_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermZec);
            arrayPerm_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermKreide);
            arrayPerm_[tzIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermTZ);
            arrayPerm_[quarTopIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermQuarTop);
            arrayPor_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorQuar);
            arrayPor_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorTer);
            arrayPor_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorRC);
            arrayPor_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorOli);
            arrayPor_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorOBS);
            arrayPor_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorMBS);
            arrayPor_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorSol);
            arrayPor_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorUMBS);
            arrayPor_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorUBS);
            arrayPor_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorZec);
            arrayPor_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorKreide);
            arrayPor_[tzIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorTZ);
            arrayPor_[quarTopIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorQuarTop);
            tzWidth_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.TzWidth);
            holesRupelClosed_ = GET_RUNTIME_PARAM(TypeTag, bool, SpatialParams.HolesRupelClosed);
            dirichletAtTop_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.DirichletAtTop);
            fractureOff_ = GET_RUNTIME_PARAM(TypeTag, bool, SpatialParams.FractureOff);

//            //regionionalised permeability for quarTop layer
//            //zeroth value is a dummy value for the region containing all the rest
//            permVal_[0] = 0;
//            permVal_[1] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P1Perm);
//            permVal_[2] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P2Perm);
//            permVal_[3] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P3Perm);
//            permVal_[4] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P4Perm);
//            //zeroth value is a dummy value for the region containing all the rest
//            regionCoor_[0][0] = 0;
//            regionCoor_[0][1] = 0;
//            regionCoor_[0][dimWorld-1] = 0;
//            regionCoor_[1][0] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P1XCoor);
//            regionCoor_[1][1] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P1YCoor);
//            regionCoor_[1][dimWorld-1] = 0;
//            regionCoor_[2][0] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P2XCoor);
//            regionCoor_[2][1] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P2YCoor);
//            regionCoor_[2][dimWorld-1] = 0;
//            regionCoor_[3][0] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P3XCoor);
//            regionCoor_[3][1] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P3YCoor);
//            regionCoor_[3][dimWorld-1] = 0;
//            regionCoor_[4][0] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P4XCoor);
//            regionCoor_[4][1] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P4YCoor);
//            regionCoor_[4][dimWorld-1] = 0;
//            //zeroth value is a dummy value for the region containing all the rest
//            regionRadius_[0] = 0;
//            regionRadius_[1] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P1Radius);
//            regionRadius_[2] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P2Radius);
//            regionRadius_[3] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P3Radius);
//            regionRadius_[4] = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.P4Radius);

            //get the mass reduction factor
            massReductionFactor_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MassReductionFactor);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown while reading in parameters in the spatial parameters file!\n";
            exit(1);
        }

#if MODELTYPE != 1
        // residual saturations
        materialParams_.setSwr(Swr_);
        materialParams_.setSnr(Snr_);

        // parameters for the Brooks-Corey law
        materialParams_.setPe(brooksCoreyPe_);
        materialParams_.setLambda(brooksCoreyLambda_);
#endif

        //initialize fracture faces by calling private method
        initFracFaces_();

        //initialize permeability and porosity vectors by calling private method
        initPermPor_();

        //initialize river by calling private method
        initRiver_();

        //initialize salt boundary by calling private method
        initSaltVertex_();

        //initialize salt boundary by calling private method
        initVertexTerQuar_();

#if MODELTYPE == 2
        //parameters for 2pdfm model required by the 2pdfm volume variables
        swrf_    = 0.00;
        swrm_    = 0.00;
        SnrF_    = 0.00;
        SnrM_    = 0.00;
        pdf_     = 1000; //2.5*1e4;
        pdm_     = 2000; //2.5*1e4;
        lambdaF_ = 2.0;
        lambdaM_ = 2.0;
#endif

    }

    ~SollingSpatialParams()
    {}

    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution the global solution vector
     */
//    void update(const SolutionVector &globalSolution)
//    {
//    };

    /*!
     * \brief Define the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvElemGeom,
                                 int scvIdx) const
    {

        if(this->problem().timeManager().episodeIndex() == Problem::initPressureEpsIdx &&
                Dune::FloatCmp::lt<Scalar>(perm_[this->problem().elementMapper().map(element)], initialPermeability_))
        {
            return initialPermeability_; //high permeability for creating hydrostatic conditions
        }

        return perm_[this->problem().elementMapper().map(element)];
    }

    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
        return 0;
    }

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx,
                    Scalar pressure) const
    {

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                    int dofIdxGlobal = this->problem().model().dofMapper().subIndex(element, scvIdx, dofCodim);
#else
                    int dofIdxGlobal = this->problem().model().dofMapper().map(element, scvIdx, dofCodim);
#endif

        //Get the reference porosity
        Scalar refPorosity = porosity_[this->problem().elementMapper().map(element)];

        //Return pressure independent porosity in initialization period
        if(this->problem().timeManager().episodeIndex() == Problem::initPressureEpsIdx)
        {
            refPorosity *= massReductionFactor_;
            return refPorosity;
        }

        //effective porosity calculation according to Schäfer et al. 2011
        Scalar refPressure = this->problem().initialPVMap_.find(dofIdxGlobal)->second[pressureIdx];
        Scalar X = compressibility_*(pressure - refPressure);
        Scalar effPorosity = refPorosity*(1 + X + std::pow(X,2)/2);
        return effPorosity;
    }

    /*!
     * \brief Define the fracture porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param localFaceIdx The local index of the face
     */
    Scalar fracturePorosity(const Element &element,
            const FVElementGeometry &fvGeometry,
            const int localFaceIdx) const
    {
        //Return pressure independent porosity in initialization period
        if(this->problem().timeManager().episodeIndex() == 1)
        {
            return arrayPor_[tzIdx]*massReductionFactor_;
        }
        return arrayPor_[tzIdx];
    }

    /*!
     * \brief Function for defining the intrinsic (absolute) fracture permeability.
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param localFaceIdx The index of the face.
     * \return the intrinsic fracture permeability
     */
    Scalar fracturePermeability(const Element &element,
            const FVElementGeometry &fvGeometry,
            const int localFaceIdx) const
    {
        return arrayPerm_[tzIdx];
    }

    /*!
     * \brief Function for defining the fracture width.
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param localFaceIdx The index of the face.
     * \return the fracture width
     */
    Scalar fractureWidth(const Element &element,
            const FVElementGeometry &fvGeometry,
            const int localFaceIdx) const
    {
        return tzWidth_;
    }

#if MODELTYPE == 2
    /*!
     * \brief Function for defining the parameters needed by constitutive relationships (kr-sw, pc-sw, etc.).
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return the material parameters object of the Fracture
     */
    const MaterialLawParams& fractureMaterialLawParams(const Element &element,
                                                    const FVElementGeometry &fvGeometry,
                                                    int scvIdx) const
    {
        // be picky if called for non-fracture vertices
        assert(fvGeometry.fracScv[scvIdx].isFractureScv);

        return materialParams_;
    }

    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
    {
        return materialParams_;
    }
#endif

    /*!
     * \brief Define the dispersivity.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    GlobalPosition dispersivity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        GlobalPosition dispersivity;
        dispersivity[0]= longDispersion_;
        dispersivity[1]= transDispersion_;
        dispersivity[2]= transDispersion_;
        return dispersivity;
    }

    //Check if the vertex has ter or quar layer neighbors
    bool terQuarVertex(const int vIdxGlobal) const
    {
        return terQuarVertex_[vIdxGlobal];
    }

    //Find the element belonging to a river
    bool isRiver(const Element &element, const int scvIdx) const
    {
        int vIdxGlobal = this->problem().vertexMapper().map(element, scvIdx, dofCodim);
        return riverDof_[vIdxGlobal];
    }

    //Find the element belonging to a river
    bool isRiver(const Vertex &vertex) const
    {
    	int vIdxGlobal = this->problem().vertexMapper().map(vertex);
    	return riverDof_[vIdxGlobal];
    }

    //Find the elements belonging to the flux line
    int findFluxLineIdx(const Element &element) const
    {
        std::vector<Scalar> eParam = GridCreator::template parameters<Element>(element);
        return (int)(eParam[fluxElemIdx] + 0.5);
    }

    //Check if the vertex is entirely salt
    bool saltVertex(const int vIdxGlobal) const
    {
        return saltVertex_[vIdxGlobal];
    }

    //The grid can be split into different rows of elements. The top row has index 0.
    int findElementRowIdx(const Element &element) const
    {
        std::vector<Scalar> eParam = GridCreator::template parameters<Element>(element);
        return ((int)(eParam[elemRowIdx] + 0.5));
    }

    //Find the layer index from the grid file
    int findLayerIdx(const Element &element) const
    {
        //Close the holes in the rupel if holesRupelClosed_ == true
        if(holesRupelClosed_ && findFluxLineIdx(element) == rupelFluxIdx)
            return rcIdx;

        std::vector<Scalar> eParam = GridCreator::template parameters<Element>(element);
        //return the index of the quar top layer for the first two rows
        if(((int)(eParam[elemRowIdx] + 0.5)) <= 1)
            return quarTopIdx;

        //return the index of the layer so that it fits to the array strating with 0
        return (int)(eParam[layerEIdx] + 0.5) - 1;
    }

#if MODELTYPE == 2
    //parameters for 2pdfm model required by the 2pdfm volume variables
    Scalar swrf_;
    Scalar swrm_;
    Scalar SnrF_;
    Scalar SnrM_;
    Scalar lambdaF_;
    Scalar lambdaM_;
    Scalar pdf_;
    Scalar pdm_;
#endif

private:

    //Initialize the fracture faces
    //Loop over all intersections and mark the fracture intersections with true by calling the setFractureFace() function in the
    //ParentType
    void initFracFaces_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing fracture faces."<<std::endl;
        }
        if(fractureOff_)
        {
            if(gridView_.comm().rank() == 0)
            {
                std::cout<<"Fracture turned off."<<std::endl;
            }
            return;
        }
        //Loop over all elements mark elements with interface zec/other
        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            std::vector<Scalar> eParam = GridCreator::template parameters<Element>(*eIt);
            bool eInsideInOuterDomain = this->problem().eInOuterDomain(*eIt);
            //Check if the layer idx is zec and not in outer domain
            if(((int)(eParam[layerEIdx] + 0.5) - 1) == zecIdx && !eInsideInOuterDomain)
            {
                //Loop over intersections
                IntersectionIterator isEndIt = gridView_.iend(*eIt);
                for (IntersectionIterator isIt = gridView_.ibegin(*eIt); isIt != isEndIt; ++isIt)
                {
                    //Check if element has neighbor
                    if(isIt->neighbor())
                    {
                        ElementPointer outsideEPtr(isIt->outside());
                        std::vector<Scalar> outsideEParam = GridCreator::template parameters<Element>(*outsideEPtr);
                        bool eOutsideInOuterDomain = this->problem().eInOuterDomain(*outsideEPtr);
                        int layerIdx = ((int)(outsideEParam[layerEIdx] + 0.5) - 1);
                        //Check if outside element is not zec, ubs or withing the outer domain which is a
                        //sufficient condition for a fracture face
                        if(layerIdx != zecIdx && layerIdx != ubsIdx && !eOutsideInOuterDomain)
                        {
                            //Get the local index of the face and call the ParentType to mark the face
                            int localFaceIdx = isIt->indexInInside();
                            ParentType::setFractureFace(*eIt, localFaceIdx, true);
                        }
                    }
                }
            }
        }
    }

    //Initialize the permeability and porosity.
    //Loop over all elements and obtain the permeability and porosity values from the gridPtr.
    //Store the values in the perm_ and porosity_ vectors.
    //Do some checks at the end.
    void initPermPor_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing permeabilities and porosities."<<std::endl;
        }
        int numElems = gridView_.size(0);
        perm_.resize(numElems);
        porosity_.resize(numElems);
        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            int eIdxGlobal = gridView_.indexSet().index(*eIt);
            int layerIdx = findLayerIdx(*eIt);
            perm_[eIdxGlobal] = arrayPerm_[layerIdx];
            porosity_[eIdxGlobal] = arrayPor_[layerIdx];

            // if the permeability is within one of the selected regions within the top aquifer assign the corresponding perm value
//            if(layerIdx == quarTopIdx)
//            {
//                GlobalPosition eCenter = eIt->geometry().center();
//                //Start from one to get the vector indices right
//                for(int regionIter = 1; regionIter < this->problem().numRegion_; regionIter++)
//                {
//                    if(this->problem().globalPosInRegion(eCenter, regionCoor_[regionIter], regionRadius_[regionIter]))
//                    {
//                       perm_[eIdxGlobal] = permVal_[regionIter];
//                    }
//                }
//            }
        }

        //Check maximum and minimum values in perm_/porosity_ vectors
        Scalar maxPor = 0;
        Scalar minPor = 0;
        minPor = *std::min_element(porosity_.begin(), porosity_.end());
        maxPor = *std::max_element(porosity_.begin(), porosity_.end());

        Scalar maxPerm = 0;
        Scalar minPerm = 0;
        minPerm = *std::min_element(perm_.begin(), perm_.end());
        maxPerm = *std::max_element(perm_.begin(), perm_.end());

        //Compare over different processes
        if (gridView_.comm().size() > 1)
        {
            minPor = gridView_.comm().min(minPor);
            maxPor = gridView_.comm().min(maxPor);
            maxPerm = gridView_.comm().min(maxPerm);
            minPerm = gridView_.comm().min(minPerm);
        }
        //Output only for rank 0
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Minimum Perm: "<<minPerm<<" Maximum Perm: "<<maxPerm<<std::endl;
            std::cout<<"Minimum Porosity: "<<minPor<<" Maximum Porosity: "<<maxPor<<std::endl;
        }
    }

    //Find the element or vertice belonging to a river (internal function)
    void initRiver_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing river dofs."<<std::endl;
        }
        int numDof = gridView_.size(dofCodim);
        riverDof_.resize(numDof, false);

        //Check that the Dirichlet at top property is switched off
        if(!dirichletAtTop_)
        {
            ElementIterator eIt = gridView_.template begin<0>();
            const ElementIterator eEndIt = gridView_.template end<0>();
            for (; eIt != eEndIt; ++eIt)
            {
                //Loop over intersections
                IntersectionIterator isEndIt = gridView_.iend(*eIt);
                for (IntersectionIterator isIt = gridView_.ibegin(*eIt); isIt != isEndIt; ++isIt)
                {
                    //Check if intersection on boundary
                    if(!isIt->boundary())
                        continue;

                    //Check if intersection on top boundary
                    Scalar normalZComp = isIt->centerUnitOuterNormal()[dimWorld -1];
                    Scalar absNormalZComp = std::fabs(normalZComp);
                    if(Dune::FloatCmp::gt<Scalar>(absNormalZComp, 0.2) && Dune::FloatCmp::lt<Scalar>(normalZComp, 0))
                    {
                        // box
                        if(isBox)
                        {
                            //set the fracture vertices on the face
                            Dune::GeometryType geomType =eIt->geometry().type();
                            const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
                            //Get the local index of the face and call the ParentType to mark the face
                            int localFaceIdx = isIt->indexInInside();
                            //Loop over vertices in fracture face
                            int numVerticesOfFace = referenceElement.size(localFaceIdx, 1, dim);
                            for (int vIdxInFace = 0; vIdxInFace < numVerticesOfFace; vIdxInFace++)
                            {
                                int vIdxElement = referenceElement.subEntity(localFaceIdx, /*codim face*/1, vIdxInFace, /*codim vertex*/dim);
                                int vIdxGlobal =  this->problem().vertexMapper().map(*eIt, vIdxElement, dim);
                                //Get the iterator over the neighbor elements
                                NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
                                NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
                                bool hasRiver = false;
                                bool hasNoRiver = false;
                                for(; nESIt != nESItEnd; ++nESIt)
                                {
                                    ElementPointer ePtr = this->gridView_.grid().entityPointer(*nESIt);
                                    std::vector<Scalar> eParam = GridCreator::template parameters<Element>(*ePtr);
                                    //conversion Scalar to int
                                    int riverNo = (int)(eParam[riverElemIdx] + 0.5);
                                    //we do not want river no 4!
                                    if(riverNo > 0 && riverNo != 4)
                                        hasRiver = true;
                                    else
                                        hasNoRiver = true;
                                }
                                if(hasRiver && !hasNoRiver)
                                    riverDof_[vIdxGlobal] = true;
                            }
                        }
                        //cell centered
                        else
                        {
                            int eIdxGlobal = gridView_.indexSet().index(*eIt);
                            std::vector<Scalar> eParam = GridCreator::template parameters<Element>(*eIt);
                            if(eParam[riverElemIdx] > 0.5)
                                riverDof_[eIdxGlobal] = true;
                        }
                    }
                }
            }
        }
    }

    //Check if the vertex is at the transition zone, i.e. having both zec and other layer properties (internal function)
    void initSaltVertex_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing salt vertices."<<std::endl;
        }
        int numVertex = gridView_.size(dim);
        saltVertex_.resize(numVertex, false);

        VertexIterator vIt = gridView_.template begin<dim>();
        VertexIterator vEndIt = gridView_.template end<dim>();
        for(; vIt != vEndIt; ++vIt)
        {
            int vIdxGlobal = this->problem().vertexMapper().map(*vIt);

            //Get the iterator over the neighbor elements
            NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
            NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
            bool hasSalt = false;
            bool hasNoSalt = false;
            for(; nESIt != nESItEnd; ++nESIt)
            {
                ElementPointer ePtr = this->gridView_.grid().entityPointer(*nESIt);
                int layerIdx = findLayerIdx(*ePtr);
                if(layerIdx == zecIdx)
                    hasSalt = true;

                if(layerIdx != zecIdx)
                    hasNoSalt = true;
            }
            //vertices within the salt layer
            if(hasSalt && !hasNoSalt)
                saltVertex_[vIdxGlobal] = true;
            //outside of salt layer or on the salt border
            else
                saltVertex_[vIdxGlobal] = false;

        }
    }

    //Check if the vertex has ter or quar layer neighbors (internal function)
    void initVertexTerQuar_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing ter or quar vertices."<<std::endl;
        }
        int numVertex = gridView_.size(dim);
        terQuarVertex_.resize(numVertex, false);

        VertexIterator vIt = gridView_.template begin<dim>();
        VertexIterator vEndIt = gridView_.template end<dim>();
        for(; vIt != vEndIt; ++vIt)
        {
            int vIdxGlobal = this->problem().vertexMapper().map(*vIt);
            //Get the iterator over the neighbor elements
            NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
            NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
            bool hasTerQuar = false;
            for(; nESIt != nESItEnd; ++nESIt)
            {
                ElementPointer ePtr = this->gridView_.grid().entityPointer(*nESIt);
                std::vector<Scalar> eParam = GridCreator::template parameters<Element>(*ePtr);
                int layerIdx = ((int)(eParam[layerEIdx] + 0.5) - 1);
                if(layerIdx == terIdx || layerIdx == quarIdx || layerIdx == quarTopIdx)
                    hasTerQuar = true;

            }
            if(hasTerQuar)
                terQuarVertex_[vIdxGlobal] = true;
        }
    }

public:
    // ReservoirProperties
    static constexpr int numLayer_ = 13;
    Scalar arrayTopLayer_[numLayer_];
    Scalar topOfReservoir_;
    Scalar tzWidth_;
    Scalar initialPermeability_;
    Scalar arrayPerm_[numLayer_];
    Scalar arrayPor_[numLayer_];

private:
    //Vectors storing permeability and porosity values at every element for faster access
    PermeabilityType perm_;
    PorosityType porosity_;
    Scalar longDispersion_;
    Scalar transDispersion_;

    bool holesRupelClosed_;
    bool dirichletAtTop_;
    bool fractureOff_;

    //Two phase parameters
#if MODELTYPE != 1
    MaterialLawParams materialParams_;
#endif
    Scalar Swr_;
    Scalar Snr_;
    Scalar brooksCoreyPe_;
    Scalar brooksCoreyLambda_;
    Scalar compressibility_;

    //gridPtr_ for accessing parameters in dgf
    const GridView gridView_;

    //Initialization period
    Scalar initializationPeriod_;


    //Mass reduction factor for multiplication with porosity
    Scalar massReductionFactor_;

    std::vector<bool> quarTopElement_;
    std::vector<bool> riverDof_;
    std::vector<bool> terQuarVertex_;
    std::vector<bool> saltVertex_;

    // Mapper for assigning neighbor elements (value) to global vertex indices
    const VertIdxToElemNeighborMapper vertexElementMapper_;

    //regionalised permeability for quarTop layer
//    Dune::FieldVector<Scalar, Problem::numRegion_> permVal_;
//    GlobalPosition regionCoor_[Problem::numRegion_];
//    Scalar regionRadius_[Problem::numRegion_];
};

}

#endif
