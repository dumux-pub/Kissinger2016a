#ifndef DUNE_COMPLEX_DGFEXTENDER_HH
#define DUNE_COMPLEX_DGFEXTENDER_HH

/** \file 
 *  \brief write a GridView to a DGF file
 *  \author Martin Nolte
 */

#include <fstream>
#include <vector>

#include <dune/common/float_cmp.hh>
#include <dune/grid/common/grid.hh>
#include <dune/grid/io/file/vtk/boundaryiterators.hh>
#include <dune/common/float_cmp.hh>
//#include <dumux/common/basicproperties.hh>
#include "dgfextender.hh"


namespace Dumux
{

template <class TypeTag>
class GenericDGFExtender;

namespace Properties
{
//NEW_PROP_TAG(DGFExtender); //! The type of the dgfextender
//NEW_TYPE_TAG(GenericDGFExtender, INHERITS_FROM(NumericModel));
//SET_TYPE_PROP(GenericDGFExtender,
//              DGFExtender,
//              Dumux::GenericDGFExtender<TypeTag>);
}

  /** \class DGFExtender
   *  \ingroup DuneGridFormatParser
   *  \brief write a GridView to a DGF file
   *
   *  The DGFExtender allows create a DGF file from a given GridView. It allows
   *  for the easy creation of file format converters.
   *
   *  \tparam  GV  GridView to write in DGF format
   */
  template< class TypeTag >
  class ComplexDGFExtender : public DGFExtender<TypeTag>
  {
    typedef GenericDGFExtender< TypeTag > This;
    typedef Dumux::DGFExtender< TypeTag > ParentType;

  public:
    /** \brief type of grid view */
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    /** \brief type of underlying hierarchical grid */
    typedef typename GridView::Grid Grid;
    typedef Dune::GridPtr<Grid> GridPointer;

    /** \brief dimension of the grid */
    static const int dim = GridView::dimension;
    static const int dimGrid = GridView::dimension;

  private:
    typedef typename GET_PROP_TYPE(TypeTag, DGFExtender) Implementation;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
	typedef typename GridView::Intersection Intersection;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim< 0 >::Iterator ElementIterator;
    typedef typename GridView::template Codim< dimGrid >::Iterator VertexIterator;
    typedef Dune::VTK::BoundaryIterator<GridView> BoundaryIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::ctype CoordScalar;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;
    typedef typename IndexSet::IndexType Index;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimGrid> GlobalPosition;

    enum {
        //Layer indices
        quarIdx=0,
        terIdx=1,
        rcIdx=2,
        oliIdx=3,
        obsIdx=4,
        mbsIdx=5,
        solIdx=6,
        umbsIdx=7,
        ubsIdx=8,
        zecIdx=9,
        kreideIdx=10,
        tzIdx=11,
        quarTopIdx = 12,
    };

    enum {
        //indices of element parameters specified in dgf file
        fluxElemIdx = 0,
        layerEIdx = 1,
        riverElemIdx = 2,
        elemRowIdx = 3,
        outerDomain = 4
    };



  public:

    /** \brief constructor
     *
     *  \param[in]  gridView  grid view to operate on
     */
    ComplexDGFExtender ( const GridView &gridView, GridPointer &gridPtr )
    : ParentType(gridView, gridPtr)
    {
        extendLayer_ = GET_RUNTIME_PARAM(TypeTag, std::vector<int>, SpatialParams.ExtendLayer);
        arrayRowIdxBotLayer_[quarTopIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotQuarTop);
        arrayRowIdxBotLayer_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotQuar);
        arrayRowIdxBotLayer_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotTer);
        arrayRowIdxBotLayer_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotRC);
        arrayRowIdxBotLayer_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotOli);
        arrayRowIdxBotLayer_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotOBS);
        arrayRowIdxBotLayer_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotMBS);
        arrayRowIdxBotLayer_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotSol);
        arrayRowIdxBotLayer_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotUMBS);
        arrayRowIdxBotLayer_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotUBS);
        arrayRowIdxBotLayer_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotZec);
        arrayRowIdxBotLayer_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.RowIdxBotKreide);
    }

    //Check whether the boundary intersection is marked for extension
    bool extendIs(const Element element, const Intersection is) const
    {
        if(!is.boundary())
            DUNE_THROW( Dune::GridError, "Interior intersection marked for extension!" );

        //No elements on the top and bottom boundaries
        int intersectionIdx = is.indexInInside();
        if(intersectionIdx == ParentType::topIdx || intersectionIdx == ParentType::bottomIdx)
            return false;

        for (int layerIter = 0; layerIter < extendLayer_.size(); ++layerIter)
        {
            if(initialLayerIdx(element) == extendLayer_[layerIter])
            {
                return true;
            }
        }
        return false;
    }

    //get parameters for elements
    void getEParameters(std::vector<Scalar> &eParam, const Element &element, bool extensionE) const
    {
        //Call the parent type implementation of this function
        ParentType::getEParameters(eParam, element, extensionE);
        //Manipulate parameters for outer domain
        if(extensionE)
        {
            if(eParam[layerEIdx] == oliIdx + 1)
                eParam[layerEIdx] = oliIdx + 1;
            else if(eParam[layerEIdx] == zecIdx + 1)
                eParam[layerEIdx] = zecIdx + 1;
            else
                eParam[layerEIdx] = initialLayerIdx(element) + 1;
            eParam[riverElemIdx] = 0.0;
            eParam[fluxElemIdx] = 0.0;
        }
    }


  protected:
    Implementation &asImp_()
    {
        assert(static_cast<Implementation*>(this) != 0);
        return *static_cast<Implementation*>(this);
    }

    const Implementation &asImp_() const
    {
        assert(static_cast<const Implementation*>(this) != 0);
        return *static_cast<const Implementation*>(this);
    }

  private:

    //return the layer index according to the layer bottom (internal function)
    int initialLayerIdx(const Element &element) const
    {
        int rowIdx = (*this->gridPtr_).template parameters<Element>(element)[elemRowIdx];
        if(rowIdx <= arrayRowIdxBotLayer_[quarTopIdx])
            return quarTopIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[quarIdx] && rowIdx > arrayRowIdxBotLayer_[quarTopIdx])
            return quarIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[terIdx] && rowIdx > arrayRowIdxBotLayer_[quarIdx])
            return terIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[rcIdx] && rowIdx > arrayRowIdxBotLayer_[terIdx])
            return rcIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[oliIdx] && rowIdx > arrayRowIdxBotLayer_[rcIdx])
            return oliIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[kreideIdx] && rowIdx > arrayRowIdxBotLayer_[oliIdx])
            return kreideIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[obsIdx] && rowIdx > arrayRowIdxBotLayer_[kreideIdx])
            return obsIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[mbsIdx] && rowIdx > arrayRowIdxBotLayer_[obsIdx])
            return mbsIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[solIdx] && rowIdx > arrayRowIdxBotLayer_[mbsIdx])
            return solIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[umbsIdx] && rowIdx > arrayRowIdxBotLayer_[solIdx])
            return umbsIdx;
        else if(rowIdx <= arrayRowIdxBotLayer_[ubsIdx] && rowIdx > arrayRowIdxBotLayer_[umbsIdx])
            return ubsIdx;
        else
            return zecIdx;
    }
    static constexpr int numLayer_ = 13;
    Scalar arrayRowIdxBotLayer_[numLayer_];
    std::vector<int> extendLayer_;
  };
}

#endif // #ifndef DUNE_DGFWRITER_HH
