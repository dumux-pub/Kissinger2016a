#ifndef DUNE_GENERIC_DGFEXTENDER_HH
#define DUNE_GENERIC_DGFEXTENDER_HH

/** \file 
 *  \brief write a GridView to a DGF file
 *  \author Martin Nolte
 */

#include <fstream>
#include <vector>

#include <dune/common/float_cmp.hh>
#include <dune/grid/common/grid.hh>
#include <dune/grid/io/file/vtk/boundaryiterators.hh>
#include <dune/common/float_cmp.hh>
//#include <dumux/common/basicproperties.hh>
#include "dgfextender.hh"


namespace Dumux
{

template <class TypeTag>
class GenericDGFExtender;

namespace Properties
{
//NEW_PROP_TAG(DGFExtender); //! The type of the dgfextender
//NEW_TYPE_TAG(GenericDGFExtender, INHERITS_FROM(NumericModel));
//SET_TYPE_PROP(GenericDGFExtender,
//              DGFExtender,
//              Dumux::GenericDGFExtender<TypeTag>);
}

  /** \class DGFExtender
   *  \ingroup DuneGridFormatParser
   *  \brief write a GridView to a DGF file
   *
   *  The DGFExtender allows create a DGF file from a given GridView. It allows
   *  for the easy creation of file format converters.
   *
   *  \tparam  GV  GridView to write in DGF format
   */
  template< class TypeTag >
  class GenericDGFExtender : public DGFExtender<TypeTag>
  {
    typedef GenericDGFExtender< TypeTag > This;
    typedef Dumux::DGFExtender< TypeTag > ParentType;

  public:
    /** \brief type of grid view */
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    /** \brief type of underlying hierarchical grid */
    typedef typename GridView::Grid Grid;
    typedef Dune::GridPtr<Grid> GridPointer;

    /** \brief dimension of the grid */
    static const int dim = GridView::dimension;
    static const int dimGrid = GridView::dimension;

  private:
    typedef typename GET_PROP_TYPE(TypeTag, DGFExtender) Implementation;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Entity Element;
	typedef typename GridView::Intersection Intersection;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim< 0 >::Iterator ElementIterator;
    typedef typename GridView::template Codim< dimGrid >::Iterator VertexIterator;
    typedef Dune::VTK::BoundaryIterator<GridView> BoundaryIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::ctype CoordScalar;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;
    typedef typename IndexSet::IndexType Index;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimGrid> GlobalPosition;

    enum {
        //Layer indices
        quarIdx=0,
        terIdx=1,
        rcIdx=2,
        oliIdx=3,
        obsIdx=4,
        mbsIdx=5,
        solIdx=6,
        umbsIdx=7,
        ubsIdx=8,
        zecIdx=9,
        kreideIdx=10,
        tzIdx=11,
        quarTopIdx = 12,
    };

  public:

    /** \brief constructor
     *
     *  \param[in]  gridView  grid view to operate on
     */
    GenericDGFExtender ( const GridView &gridView, GridPointer &gridPtr )
    : ParentType(gridView, gridPtr)
    {
        extendLayer_ = GET_RUNTIME_PARAM(TypeTag, std::vector<int>, SpatialParams.ExtendLayer);
        arrayBotLayer_[quarTopIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotQuarTop);
        arrayBotLayer_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotQuar);
        arrayBotLayer_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotTer);
        arrayBotLayer_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotRC);
        arrayBotLayer_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotOli);
        arrayBotLayer_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotOBS);
        arrayBotLayer_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotMBS);
        arrayBotLayer_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotSol);
        arrayBotLayer_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotUMBS);
        arrayBotLayer_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotUBS);
        arrayBotLayer_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotZec);
        arrayBotLayer_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotKreide);
    }

    //Check whether the boundary intersection is marked for extension
    bool extendIs(const Element element, const Intersection is) const
    {
        if(!is.boundary())
            DUNE_THROW( Dune::GridError, "Interior intersection marked for extension!" );

        //No elements on the top and bottom boundaries
        int intersectionIdx = is.indexInInside();
        if(intersectionIdx == ParentType::topIdx || intersectionIdx == ParentType::bottomIdx)
            return false;


        for (int layerIter = 0; layerIter < extendLayer_.size(); ++layerIter)
        {
            if(initialLayerIdx(element) == extendLayer_[layerIter])
            {
                return true;
            }
        }

        return false;
    }

  protected:
    Implementation &asImp_()
    {
        assert(static_cast<Implementation*>(this) != 0);
        return *static_cast<Implementation*>(this);
    }

    const Implementation &asImp_() const
    {
        assert(static_cast<const Implementation*>(this) != 0);
        return *static_cast<const Implementation*>(this);
    }
  private:

    //return the layer index according to the layer bottom (internal function)
    int initialLayerIdx(const Element &element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        if(globalPos[dim-1] < arrayBotLayer_[quarTopIdx])
            return quarTopIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[quarIdx] && globalPos[dim-1] > arrayBotLayer_[quarTopIdx])
            return quarIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[terIdx] && globalPos[dim-1] > arrayBotLayer_[quarIdx])
            return terIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[rcIdx] && globalPos[dim-1] > arrayBotLayer_[terIdx])
            return rcIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[oliIdx] && globalPos[dim-1] > arrayBotLayer_[rcIdx])
            return oliIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[kreideIdx] && globalPos[dim-1] > arrayBotLayer_[oliIdx])
            return kreideIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[obsIdx] && globalPos[dim-1] > arrayBotLayer_[kreideIdx])
            return obsIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[mbsIdx] && globalPos[dim-1] > arrayBotLayer_[obsIdx])
            return mbsIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[solIdx] && globalPos[dim-1] > arrayBotLayer_[mbsIdx])
            return solIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[umbsIdx] && globalPos[dim-1] > arrayBotLayer_[solIdx])
            return umbsIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[ubsIdx] && globalPos[dim-1] > arrayBotLayer_[umbsIdx])
            return ubsIdx;
        else
            return zecIdx;
    }
    static constexpr int numLayer_ = 13;
    Scalar arrayBotLayer_[numLayer_];
    std::vector<int> extendLayer_;

  };
}

#endif // #ifndef DUNE_DGFWRITER_HH
