// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by                                                   *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
* \file
*
* \brief Adaption of the box scheme to the two-phase flow model.
*/

#ifndef DUMUX_SOLLING_1P2CNI_MODEL_HH
#define DUMUX_SOLLING_1P2CNI_MODEL_HH

#include <dumux/implicit/dfm/1p2cdfm/1p2cdfmmodel.hh>
#include "../helperclasses/vertidxtoelemneighbormapper.hh"

namespace Dumux
{

namespace Properties
{
//Declare new property
NEW_PROP_TAG(FluxVector);

//Set default value
SET_PROP(OnePTwoC, FluxVector)
{
    //private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases)
    };
public:
    typedef Dune::FieldVector<Scalar, numPhases + numEq> type;
};
}

template<class TypeTag >
class SollingOnePTwoCNIModel : public OnePTwoCDFMModel<TypeTag>
{
	typedef OnePTwoCDFMModel<TypeTag> ParentType;
	typedef typename GET_PROP_TYPE(TypeTag, BaseModel) BaseType;
	typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
	typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
	typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
	typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
	typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
	typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
	typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
	typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

	enum {
			numPhases = GET_PROP_VALUE(TypeTag, NumPhases)
		};

		typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
		enum {
	        pressureIdx = Indices::pressureIdx,
	//        saturationIdx = Indices::saturationIdx,
	        pwIdx = Indices::pressureIdx,
	        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
	//        SnIdx = Indices::snIdx,

	        // equation indices
	        contiWEqIdx = Indices::conti0EqIdx,
	        transportEqIdx = Indices::transportEqIdx,
	//        contiNEqIdx = Indices::contiNEqIdx,

	        // phase indices
	        wPhaseIdx = Indices::conti0EqIdx,
		};

		typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
		typedef typename GridView::template Codim<0>::Entity Element;
		typedef typename Element::Geometry Geometry;
	    typedef typename GridView::Intersection Intersection;
	    typedef typename GridView::IntersectionIterator IntersectionIterator;

		typedef typename GridView::template Codim<0>::Iterator ElementIterator;
		enum {
			dim = GridView::dimension
		};
		enum {
			numEq = GET_PROP_VALUE(TypeTag, NumEq)
		};
		typedef typename GridView::template Codim<dim>::Entity Vertex;
		typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

		typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
		typedef typename GridView::ctype CoordScalar;
		typedef Dune::PQkLocalFiniteElementCache<CoordScalar, Scalar, dim, 1> LocalFiniteElementCache;
		typedef typename LocalFiniteElementCache::FiniteElementType LocalFiniteElement;
	    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
	    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;

		typedef Dune::FieldVector<Scalar, numPhases> PhasesVector;
		typedef Dune::FieldVector<Scalar, dim> GlobalPosition;
		typedef Dune::FieldVector<Scalar, dim> Vector;
		typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
	    typedef Dune::FieldVector<Scalar, dim> DimVector;
		enum {
			isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox)
		};
		enum {
			dofCodim = isBox ? dim : 0
		};
	    enum { useMoles = GET_PROP_VALUE(TypeTag, UseMoles) };

	    typedef typename GridView::template Codim<dofCodim>::Entity DofEntity;
	    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
	    typedef Dumux::VertIdxToElemNeighborMapper<GridView> VertIdxToElemNeighborMapper;
	    typedef typename VertIdxToElemNeighborMapper::NeighborElementSeedsIterator NeighborElementSeedsIterator;
	    typedef typename GET_PROP_TYPE(TypeTag, FluxVector) FluxVector;


public:
// global storage only for a particular region (z coordinate increases (pos) with increasing depth in the snohvit models)
		//coordVal1 --> lower z value (above);  coordVal2 higher z value (below)
		void globalStorageCoord(PrimaryVariables &storage, int coord, Scalar coordVal1, Scalar coordVal2)
		    {
		        storage = 0;

		        ElementIterator eIt = this->gridView_().template begin<0>();
		        const ElementIterator eEndIt =this->gridView_().template end<0>();
		        for (; eIt != eEndIt; ++eIt)
		        {
		            GlobalPosition globalPosCell = eIt->geometry().center();
		            if(eIt->partitionType() == Dune::InteriorEntity)
		            {
		            if(globalPosCell[coord] > coordVal1 && globalPosCell[coord] < coordVal2) //e.g. 2300 and 3000
		            {
		                this->localResidual().evalStorage(*eIt);

		                    for (int i = 0; i < eIt->template count<dim>(); ++i)
		                       storage += this->localResidual().storageTerm()[i];
		            }
		            }
		        }
		        if (this->gridView_().comm().size() > 1)
		            storage = this->gridView_().comm().sum(storage);
		    }

        /*!
         * \brief Calculate the residual for a degree of freedom for an equation
         *
         * \param entity The entity type of the dof, i.e. element of vertex
         * \param eqIdx The equation index on the dof
         * \param pVCurrentIter The primary variable which we changed
         */
		Scalar dofResidual(const Vertex &entity, int eqIdx, Scalar pVCurrentIter)
		{
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
		    int dofIdxGlobal = this->dofMapper().index(entity);
#else
		    int dofIdxGlobal = this->method().model().dofMapper().map(entity);
#endif
		    Scalar tmp(this->curSol()[dofIdxGlobal][eqIdx]);
		    this->curSol()[dofIdxGlobal][eqIdx] = pVCurrentIter;
		    Scalar res = 0.0;
		    if(isBox)
		    {
		        const auto vertexElementMapper = this->problem_().vertexElementMapper();
		        //Loop over all elements
		        NeighborElementSeedsIterator nESIt = vertexElementMapper.vertexElementsBegin(dofIdxGlobal);
		        NeighborElementSeedsIterator nESItEnd = vertexElementMapper.vertexElementsEnd(dofIdxGlobal);
		        for(; nESIt != nESItEnd; ++nESIt)
		        {
		            ElementPointer ePtr = this->gridView_().grid().entityPointer(*nESIt);
		            int vIdx = findLocalVIdx(*ePtr, dofIdxGlobal);
		            this->localResidual().eval(*ePtr);
		            res += this->localResidual().residual(vIdx)[eqIdx];
		        }
		    }
		    this->curSol()[dofIdxGlobal][eqIdx] = tmp;
		    return res;
		}

        /*!
         * \brief Calculate the residual for a degree of freedom for an equation
         *
         * \param entity The entity type of the dof, i.e. element of vertex
         * \param eqIdx The equation index on the dof
         * \param pVCurrentIter The primary variable which we changed
         */
		Scalar dofResidual(const Element &entity, int eqIdx, Scalar pVCurrentIter)
		{
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
		    int dofIdxGlobal = this->dofMapper().index(entity);
#else
		    int dofIdxGlobal = this->method().model().dofMapper().map(entity);
#endif
		    Scalar tmp(this->curSol()[dofIdxGlobal][eqIdx]);
		    this->curSol()[dofIdxGlobal][eqIdx] = pVCurrentIter;
		    Scalar res = 0.0;

		    this->localResidual().eval(entity);
		    res = this->localResidual().residual(0)[eqIdx];

		    this->curSol()[dofIdxGlobal][eqIdx] = tmp;
		    return res;
		}

	     /*!
	      * \brief Calculate the fluxes across a certain intersection (codim = 1) in the domain.
	      * For the cc method the flux accross the intersection is calculated.
	      * For the box method only the flux over those scvf in the element that have one dof on the intersection
	      * and another dof not located on the intersection is calculated.
	      * The flux is positive if it is in the direction of the intersection's outer normal.
	      *
	      * \param flux The vector that stores the mass fluxes for every equation and the volume flux (last entry)
	      * \param element The element which contains the intersection
	      * \param is The intersection of interest
	      */
	    void calculateFluxAccrossIntersection(FluxVector &flux, const Element &element,
	    		const Intersection &is) const
	    {
	        if(element.partitionType() == Dune::InteriorEntity)
	        {
	            Scalar massUpwindWeight = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
	            if(!isBox)
	            {
	                ElementVolumeVariables elemVolVars;
	                FVElementGeometry fvElemGeom;
	                fvElemGeom.update(this->gridView_(), element);
	                //Update the element volume variables with the current solution
	                elemVolVars.update(this->problem_(), element, fvElemGeom, false /* oldSol? */);
	                //iterate over all degrees of freedom (i.e. nodes or element centers)
	                for (int scvfIdx = 0; scvfIdx < fvElemGeom.numScvf; ++scvfIdx)
	                {
	                    if(is.indexInInside() == scvfIdx)
	                    {
	                        FluxVariables vars(this->problem_(), element, fvElemGeom, scvfIdx, elemVolVars);
	                        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
	                        {
	                            // of the current phase
	                            const VolumeVariables &up =  elemVolVars[vars.upstreamIdx()];
	                            const VolumeVariables &dn =  elemVolVars[vars.downstreamIdx()];
	                                flux[contiWEqIdx] += vars.volumeFlux(phaseIdx)
	                                                                *((massUpwindWeight)*up.density()+(1 - massUpwindWeight)*dn.density());
	                                flux[transportEqIdx] += vars.volumeFlux(phaseIdx)
	                                                            *((massUpwindWeight)*up.density()*up.massFraction(massOrMoleFracIdx)
	                                                                    +(1 - massUpwindWeight)*dn.density()*dn.massFraction(massOrMoleFracIdx));
	                                flux[numEq] += vars.volumeFlux(phaseIdx);
	                        }
	                        //jump out of the scvf loop after correct scvfIdx has been found
	                        continue;
	                    }
	                }
	            }
	            else
	            {
	                FVElementGeometry fvElemGeom;
	                fvElemGeom.update(this->gridView_(), element);
	                ElementVolumeVariables elemVolVars;
	                elemVolVars.update(this->problem_(), element, fvElemGeom, false /* oldSol? */);


	//                //get the intersection center unit normal
	//                GlobalPosition isCenterUnitOuterNormal = is.centerUnitOuterNormal();
	                //iterate over all degrees of freedom (i.e. nodes or element centers)
	                for (int scvfIdx = 0; scvfIdx < fvElemGeom.numScvf; ++scvfIdx)
	                {
	//                    GlobalPosition scvfCenterUnitOuterNormal = fvElemGeom.subContVolFace[scvfIdx].normal;
	//                    scvfCenterUnitOuterNormal /= fvElemGeom.subContVolFace[scvfIdx].normal.two_norm();
	                    FluxVector tmpFlux(0);
	                    int scvIdxI = fvElemGeom.subContVolFace[scvfIdx].i;
	                    int scvIdxJ = fvElemGeom.subContVolFace[scvfIdx].j;
	                    bool iOnIntersection = scvIdxIsOnIntersection(scvIdxI, is, element);
	                    bool jOnIntersection = scvIdxIsOnIntersection(scvIdxJ, is, element);
	                    int multiplier;
	                    if(iOnIntersection && !jOnIntersection)
	                        multiplier = -1;
	                    else if(!iOnIntersection && jOnIntersection)
	                        multiplier = 1;
	                    else
	                        multiplier = 0;


	                    if(multiplier != 0)
	                    {
	                        FluxVariables vars(this->problem_(), element, fvElemGeom, scvfIdx, elemVolVars);

	                        const VolumeVariables &up =  elemVolVars[vars.upstreamIdx()];
	                        const VolumeVariables &dn =  elemVolVars[vars.downstreamIdx()];
	                        int phaseIdx = 0;

	                        tmpFlux[contiWEqIdx] += vars.volumeFlux(phaseIdx)
	                                *((massUpwindWeight)*up.density()+(1 - massUpwindWeight)*dn.density());
	                        tmpFlux[contiWEqIdx] += vars.fractureVolumeFlux(phaseIdx)
	                                *((massUpwindWeight)*up.density()+(1 - massUpwindWeight)*dn.density());
	                        tmpFlux[transportEqIdx] += vars.volumeFlux(phaseIdx)
	                                *((massUpwindWeight)*up.density()*up.massFraction(massOrMoleFracIdx)
	                                +(1 - massUpwindWeight)*dn.density()*dn.massFraction(massOrMoleFracIdx));
	                        tmpFlux[transportEqIdx] += vars.fractureVolumeFlux(phaseIdx)
	                                *((massUpwindWeight)*up.density()*up.massFraction(massOrMoleFracIdx)
	                                +(1 - massUpwindWeight)*dn.density()*dn.massFraction(massOrMoleFracIdx));

	                        //Volume flux
	                        tmpFlux[numEq + phaseIdx] += vars.volumeFlux(phaseIdx);
	                        tmpFlux[numEq + phaseIdx] += vars.fractureVolumeFlux(phaseIdx);

	                        //Calculate and add dispersive salt flux
	                        computeDiffusiveFlux(tmpFlux, vars);

	                    }
	                    tmpFlux *= multiplier;
	                    flux += tmpFlux;
	                }
	            }
	        }
	    }

	    /*!
	      * \brief Check whether an scv lies on an intersection
	      *
	      * \param scvIdx Local index of the vertex in the element
	      * \param is Intersection object
	      * \param element Element object which contains the scv and intersection
	      */
	    bool scvIdxIsOnIntersection(int scvIdx, const Intersection &is, const Element &element) const
	    {
	//        const Geometry& geometry = element.geometry();
	        Dune::GeometryType geomType = element.geometry().type();
	        const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
	        int intersectionIdx = is.indexInInside();
	        int numVerticesOfIntersection = referenceElement.size(intersectionIdx, 1, dim);
	        for (int vertInIntersection = 0; vertInIntersection < numVerticesOfIntersection; vertInIntersection++)
	        {
	            int vertInElementIdx = referenceElement.subEntity(intersectionIdx, 1, vertInIntersection, dim);
	            if(vertInElementIdx == scvIdx)
	                return true;
	        }
	        return false;
	    }

	    /*!
	     * \brief Adds the diffusive mass flux of all components over
	     *        a face of a sub-control volume.
	     *
	     * \param flux The diffusive flux over the sub-control-volume face for each component
	     * \param fluxVars The flux variables at the current SCV
	     */
	    void computeDiffusiveFlux(FluxVector &flux, const FluxVariables &fluxVars) const
	    {
	        Scalar tmp(0);

            // diffusive flux of the second component - massfraction
	            tmp = -(fluxVars.moleFractionGrad(transportEqIdx)*fluxVars.face().normal);
	            tmp *= fluxVars.porousDiffCoeff() * fluxVars.molarDensity();

	            // dispersive flux of second component - massfraction
	                       GlobalPosition normalDisp;
	                       fluxVars.dispersionTensor().mv(fluxVars.face().normal, normalDisp);
	                       tmp -= fluxVars.molarDensity()*
	                       (normalDisp * fluxVars.moleFractionGrad(transportEqIdx));

	            // convert it to a mass flux and add it
	            flux[transportEqIdx] += tmp * FluidSystem::molarMass(transportEqIdx);
	    }


     /*!
      * \brief Calculate the fluxes across a certain layer in the domain.
      * The layer is situated perpendicular to the coordinate axis "coord" and cuts
      * the axis at the value "coordVal".
      *
      * \param globalSol The global solution vector
      * \param flux A vector to store the flux
      * \param axis The dimension, perpendicular to which the layer is situated
      * \param coordVal The (Scalar) coordinate on the axis, at which the layer is situated
      */
     void calculateFluxAcrossLayer(PrimaryVariables &flux, int coord,
             Scalar coordVal)
     {
         //Scalar massUpwindWeight = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
         Scalar massUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
         ElementVolumeVariables elemVolVars;
         FVElementGeometry fvGeometry;

         ElementIterator elemIt = this->gridView_().template begin<0>();
         const ElementIterator &endit = this->gridView_().template end<0> ();

         GlobalPosition globalI, globalJ;
         PrimaryVariables tmpFlux(0.0);
         int sign;
//         DimVector normalPlane_;
//         normalPlane_=0;

         // Loop over elements
         for (; elemIt != endit; ++elemIt)
         {
            if(elemIt->partitionType() == Dune::InteriorEntity){
             fvGeometry.update(this->gridView_(), *elemIt);
             elemVolVars.update(this->problem_(),
                     *elemIt,
                     fvGeometry,
                     false /* oldSol? */);
             if (elemIt->partitionType() != Dune::InteriorEntity)
                 continue;

             for (int faceId = 0; faceId< fvGeometry.numEdges; faceId++)
             {
                 int idxI = fvGeometry.subContVolFace[faceId].i;
                 int idxJ = fvGeometry.subContVolFace[faceId].j;
                 int flagI, flagJ;

                 globalI = fvGeometry.subContVol[idxI].global;
                 globalJ = fvGeometry.subContVol[idxJ].global;

                 // 2D case: give y or x value of the line over which flux is to be
                 //            calculated.
                 // up to now only flux calculation to lines or planes (3D) parallel to
                 // x, y and z axis possible

                 // Flux across plane with z = 80 numEq
                 if (globalI[coord] < coordVal)
                     flagI = 1;
                 else
                     flagI = -1;

                 if (globalJ[coord] < coordVal)
                     flagJ = 1;
                 else
                     flagJ = -1;

                 if (flagI == flagJ)
                 {
                     sign = 0;
                 }
                 else
                 {
                     if (flagI > 0)
                         sign = -1;
                     else
                         sign = 1;
                 }
                 // get variables
                 if (flagI != flagJ)
                 {
                     FluxVariables fluxVars(this->problem_(),
                             *elemIt,
                             fvGeometry,
                             faceId,
                             elemVolVars);
                     tmpFlux = 0;

                     ////////
                     // advective fluxes of all components in all phases
                     ////////

                     // loop over all phases
                           for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
                           {
//                               normalPlane_[coord]=1;
//                              // we only want the flux orthogonal to the coord axis
//                              multiply the normal vector of the plane with the area of the face
//                               std::cout<< "normalPlane " <<normalPlane_[0] << normalPlane_[1]<<normalPlane_[2]<< std::endl;
//                               std::cout<< "area " <<fluxVars.face().area<< std::endl;
//                                normalPlane_*=fluxVars.face().area;
//                                DimVector normalVectorMultiplication_=normalPlane_*fluxVars.face().area;
//                                std::cout<< "velocity " << fluxVars.velocity(phaseIdx)<< std::endl;
//                                Scalar volumeFlux = fluxVars.velocity(phaseIdx)*normalPlane_;
//                                std::cout<< "normalPlanenew " <<normalPlane_[0] << normalPlane_[1]<<normalPlane_[2]<< std::endl;
//                                std::cout<< "VolumeFlux " <<volumeFlux<< std::endl;

                               // data attached to upstream and the downstream vertices
                               // of the current phase
                               const VolumeVariables &up = elemVolVars[fluxVars.upstreamIdx(phaseIdx)];
                               const VolumeVariables &dn = elemVolVars[fluxVars.downstreamIdx(phaseIdx)];

                               // add advective flux of current phase
                              // int eqIdx = (phaseIdx == wPhaseIdx) ? contiWEqIdx : contiNEqIdx;
                               tmpFlux[phaseIdx] +=
                                   fluxVars.volumeFlux(phaseIdx)
                                  *
                                   ((    massUpwindWeight_)*up.density(phaseIdx)
                                    +
                                    (1 - massUpwindWeight_)*dn.density(phaseIdx));
                           }


                     // the face normal points into the outward direction, so we
                     // have to multiply all fluxes with -1
                     tmpFlux *= -1;

                     // this->localResidual().computeFlux(tmpFlux, faceId);
                     tmpFlux *= sign;
                     flux += tmpFlux;
                 }
             }
            }
         }
         //If parallel sum of fluxes over all processors
         if (this->gridView_().comm().size() > 1)
         flux = this->problem_().gridView().comm().sum(flux);
     }

private:

     int findLocalVIdx(const Element &element, int vIdxGlobal )
     {
         for (int scvIdx = 0; scvIdx < element.subEntities(dim); ++scvIdx)
         {
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
             int idx = this->vertexMapper().subIndex(element, scvIdx, dim);
#else
             int idx = this->vertexMapper().map(element, scvIdx, dim);
#endif
             if(idx == vIdxGlobal)
                 return scvIdx;
         }
         DUNE_THROW(Dune::NotImplemented,
                    "Could not global vertex index in element");
     }
    const LocalFiniteElementCache feCache_;

};

}

#include <dumux/implicit/dfm/1p2cdfm/1p2cdfmpropertydefaults.hh>

#endif
