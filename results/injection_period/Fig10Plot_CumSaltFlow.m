%Plot the flux through the fault zone into the top aquifer for the complex geometry
%model using the outputReader.m.
% Scenario variable initialization

clear all;
plotName{1} = 'Fig10_cumflow_salt';

%define the indices for result and search lists
timeIdx = 1;
fluxTotalIdx = 2;
fluxQuar1Idx = 3;
fluxQuar2Idx = 4;
initTimeIdx = 5;

%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
eps = 1e4;
%array of logfiles to read
logFileName{1} = '1p2c/grad_1-0/out.log';
logFileName{2} = '1p2c/1p2c_reference/out.log';
logFileName{3} = '1p2c/grad_2-0/out.log';
% titel{1} = 'Grad NaCl = 1.0*10$^{-4}$ kg-NaCl/kg-H2O';
% titel{2} = 'Grad NaCl = 1.5*10$^{-4}$ kg-NaCl/kg-H2O';
% titel{3} = 'Grad NaCl = 2.0*10$^{-4}$ kg-NaCl/kg-H2O';
textbox{1} = 'Post-Rupelian';
textbox{2} = 'Quaternary 2';
textbox{3} = 'Quaternary 1';

titel = 'Cumulative salt flow into shallow aquifers during injection and post-injection';

legendStr{1} = 'Injection';
legendStr{2} = 'No injection';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
% strList{fluxFaultIdx} = 'Salt Flux around salt wall:';
% strList{fluxRupelIdx} = 'Salt Flux Across Rupel:';
% strList{fluxHolesIdx} = 'Salt Flux Across Holes Rupel:';
strList{fluxTotalIdx} = 'Salt flow into target aquifers [kgNaCl/s]:';
strList{initTimeIdx} = 'Initialization took:';
strList{fluxQuar1Idx} = 'Salt flow into Quar1 [kgNaCl/s]:';
strList{fluxQuar2Idx} = 'Salt flow into Quar2 [kgNaCl/s]:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter));
    flux{logIter}{fluxTotalIdx} = output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx}))* (-1);
    flux{logIter}{fluxQuar2Idx} = output{fluxQuar2Idx}(tInitIdx(logIter):length(output{fluxQuar2Idx}))* (-1);
    flux{logIter}{fluxQuar1Idx} = output{fluxQuar1Idx}(tInitIdx(logIter):length(output{fluxQuar1Idx}))* (-1);
    
    %calculate cumulative flow using the trapezoidal rule
    cumflow{logIter}{fluxTotalIdx} = trapz(time{logIter}, flux{logIter}{fluxTotalIdx})/1e9; %convert to MT
    cumflow{logIter}{fluxQuar2Idx} = trapz(time{logIter}, flux{logIter}{fluxQuar2Idx})/1e9;
    cumflow{logIter}{fluxQuar1Idx} = trapz(time{logIter}, flux{logIter}{fluxQuar1Idx})/1e9;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
Fig = figure();
set(gcf, 'Units', 'centimeters');
%specify color vector
co = [0 0 1;
      0 0.5 0;
      1 0 0];
  
%Map flux index to aquiferIter
fluxIter = [fluxTotalIdx, fluxQuar2Idx, fluxQuar1Idx];

%Map of categroial values for x-axis
xVal{1} = 'Low';
xVal{2} = 'Medium';
xVal{3} = 'High';
xVal{4} = xVal{1};
xVal{5} = xVal{2};
xVal{6} = xVal{3};
xVal{7} = xVal{1};
xVal{8} = xVal{2};
xVal{9} = xVal{3};

counter = 0;
offset = 0.18;
%Loop over aquifers
for aquiferIter=1:3
    %Loop over the log-files
    for logIter = 1:length(logFileName)
        counter = counter + 1;
        fluxplot = plot(counter, cumflow{logIter}{fluxIter(aquiferIter)}, 'kx', ...
            counter, flux{logIter}{fluxIter(aquiferIter)}(1)*100*365*24*3600/1e9, 'ko');
        set(fluxplot, 'LineWidth',1.0);
        hold on;
    end
    %Plot vertical line
    if(aquiferIter < 3)
        plot([counter + 0.5 counter + 0.5], get(gca, 'ylim'), 'k')
    end
    %Set text boxes
    h_anno = annotation('textbox', [offset, 0.85, 0.15, 0.05], 'string', textbox{aquiferIter});%, 'FitBoxToText','on');
    set(h_anno,'Interpreter',strInterpreter)
    offset = offset + 0.25;
    hold on;
end
axis([0.5,9.5,0.0,2.5])
set(gca, 'XTick', 1:9);
set(gca,'XTickLabel',xVal)
h_leg = legend(legendStr, 'Position', [0.73,0.6,0.1,0.2]);
set(h_leg,'Interpreter',strInterpreter, 'FontSize', 9);
h_title = title(titel,'FontSize', iFontSize);
set(h_title,'Interpreter',strInterpreter)
ylabel('Cum. salt mass [Mt-NaCl]', 'FontSize', iFontSize, 'Interpreter',strInterpreter);
xlabel('Scenario Name', 'FontSize', iFontSize, 'Interpreter',strInterpreter);
% Set the font size of the axis ticks
set(gca,'FontSize',tickFontSize)
set(fluxplot, 'LineWidth',1);

set(gcf, 'Units','centimeters', 'Position',[0 0 18 9])
set(gcf, 'PaperPositionMode','auto')
saveas(Fig,plotName{1},'epsc')
