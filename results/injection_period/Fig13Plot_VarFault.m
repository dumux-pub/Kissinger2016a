%Plot the flux into the top aquifer for the complex geometry
%model using the outputReader.m
% scenario variable permeability of transition zone (Störungszone)

clear all;
plotName = 'Fig13_varfault';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;
initTimeIdx = 6;
eps = 1e4;
injectionRate = 24.9;

%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 19, 17]); 

% Folder names
folderName = '1p2c/fault/';

%Scenario name 
scenarioName{1} = 'bar1e-18';
scenarioName{2} = 'bar1e-20';

% Title names
titel{1} = 'Barrier rock perm. = 10$^{-18}$ m$^2$';
titel{2} = 'Barrier rock perm. = 10$^{-20}$ m$^2$';

%initalization time and injection rate, check from dumux input file or end
%of log-file
%array of logfiles to read
logFileName{1} = '_f1e-11.log';
logFileName{2} = '_f1e-12.log';
logFileName{3} = '_f1e-13.log';
logFileName{4} = '_f1e-14.log';
logFileName{5} = '_f1e-15.log';
logFileName{6} = '_f1e-16.log';
logFileName{7} = '_f1e-17.log';
logFileName{8} = '_f1e-18.log';
logFileName{9} = '_f1e-19.log';
logFileName{10} = '_f1e-20.log';
% X-Axis for subplots
perm = [1e-11 1e-12 1e-13 1e-14 1e-15 1e-16 1e-17 1e-18 1e-19 1e-20];

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Total mass flow around salt wall [kg/s]:';
strList{fluxRupelIdx} = 'Total mass flow across Rupel [kg/s]:';
strList{fluxHolesIdx} = 'Total mass flow across windows Rupel [kg/s]:';
strList{fluxTotalIdx} = 'Total mass flow into target aquifers [kg/s]:';
strList{initTimeIdx} = 'Initialization took:';

%colors
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';

%Change folder
cd(folderName);
%Loop over foldername and make a subplot for each folder
for scenarioIter=1:length(scenarioName)
    %calculate the fluxes use the function outputReader for reading the
    %log-files.
    % - Loop over the nuber of log-files to be evaluated
    % - Subract the base flux at tinit from all fluxes, multiply by -1
    %   to give the flux a positive value;
    % - divide by the injection rate to make
    % - the flux dimensionless
    for logIter = 1:length(logFileName)
        output = outputReader(strList, [scenarioName{scenarioIter}, logFileName{logIter}]);
        tinit(logIter) = output{initTimeIdx};
        tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
        time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600;
        fluxTotal{logIter} = (output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx})))*(-1)/injectionRate;
        fluxHoles{logIter} = (output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx})))*(-1)/injectionRate;
        fluxFault{logIter} = (output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx})))*(-1)/injectionRate;
        maxFluxFault(logIter) = max(fluxFault{logIter});
        maxFluxTotal(logIter) = max(fluxTotal{logIter});
        maxFluxHoles(logIter) = max(fluxHoles{logIter});
    end
    
    subplot(1,2,scenarioIter);
    fluxplot = semilogx(perm,maxFluxTotal, sy{1}, perm, maxFluxFault, sy{2}, perm, maxFluxHoles, sy{3});
    set(fluxplot, 'LineWidth',1.5);
    h_title = title(titel{scenarioIter},'FontSize', iFontSize,'Interpreter',strInterpreter);
    ylabel('Max. brine flow / injection rate [-]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    xlabel('Perm. fault zone [m$^2$]', 'Interpreter',strInterpreter, 'FontSize', iFontSize);
    if(scenarioIter==1)
        h_leg = legend('Total brine flow', ...
            'Flow over fault zone', ...
            'Flow over h. windows', ...
            'Location', 'NorthEast');
        set(h_leg,'Interpreter',strInterpreter, 'FontSize', 8);
    end
    axis([0,10^-11,0,1])
    axis square;
    set(gca, 'YTick', [0, 0.2, 0.4, 0.6, 0.8, 1.0]);
    set(gca, 'XTick', [1e-19, 1e-17, 1e-15, 1e-13, 1e-11]);
%     grid on;
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize);
    %Go back to original directory
end %end folder loop
cd('../..')
saveas(Fig,plotName,'epsc') 
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName, '.eps']);