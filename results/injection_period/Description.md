# Simulation folders for the injection period.

Each folder contains the input file for running the injection period of a specific scenario (Low, Medium, and High) as explained in 4.2, Scenario study 1. The main simulation output is a *out.log* file where results for each time step are stored. These files can be read with the corresponding Matlab scripts to create the plots in the paper.

**Files:**

- **FigXX.pvsm** Paraview macros for creating spatial plots in paper, simulations need to be run and the corresponding .vtu or .pvd files have to loaded

- **FigXX.m** Matlab scripts for creating 2D line plots in paper, readint out.log files for specific simulations

- **1p1c** Simulation results for 1p1c model and input files for each simulation

- **1p2c** Simulation results for 1p2c model and input files for each simulation

- **2p3c** Simulation results for 2p3c model and input files for each simulation

- **Analytical** Results of the Analytical model by Zeidouni stored in .fig files