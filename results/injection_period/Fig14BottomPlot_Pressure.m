%Calculate the pressure increase at two locations M1 6.2 km from injection and M2
%13.5 km from injection for all models - low diffuse leakage scenario
%k=1e-20 m2

clear all;
plotName{1} = 'Fig14Bottom_Compall_Pressure_k1e20';

%define the indices for result and search lists
timeIdx = 1;
fluxTotalIdx = 2;
fluxFaultIdx = 3;
fluxHolesIdx = 4;
M1Idx = 5;
M2Idx = 6;
initTimeIdx = 8;

%Volumetric injection rate [m3/s], injection rate [kg/s]
injectionRateCO2 = 15.86; %[kg/s] CO2 injection rate
iPCO2Density = 686.5; % [kg/m3] initial CO2 density injection point reference case
iPBrineDensity = 1078; % [kg/m3] initial brine density injection point reference case
volInjectionRate = injectionRateCO2/iPCO2Density; % [m3/s]

%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
eps = 1e4;
%array of logfiles to read
logFileName{1} = '2p3c/2p3c_Focused/out.log';
logFileName{2} = '1p2c/bar1e-20/out.log';
logFileName{3} = '1p1c/1p1c_Focused/out.log';
% logFileName{4} = 'generic_bar1e-20.log';


% % Get analytical solution from .fig 
% openfig('analyticalD_average.fig', 'invisible');
% data=get(gca,'Children');
% xAnalyticalD=get(data,'XData'); %get the x data 
% %get the y data,
% %multiply with volumetric injection rate and density of 1p2c reference at injection
% yAnalyticalD=get(data,'YData'); 

titel{1} = 'Pressure at M1 - 6.2 km from IP';
titel{2} = 'Pressure at M2 - 13.5 km from IP';
legendStr{1} = '2p3c';
legendStr{2} = '1p2c';
legendStr{3} = '1p1c';
% legendStr{4} = 'Generic 1p2c';
legendStr{4} = 'Analytical';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Volume flow around salt wall [m3/s]:';
strList{fluxHolesIdx} = 'Volume flow across windows Rupel [m3/s]:';
strList{fluxTotalIdx} = 'Volume flow into target aquifers [m3/s]:';
strList{M1Idx} = 'Measurement Point 1 Pw [Pa]:';
strList{M2Idx} = 'Measurement Point 2 Pw [Pa]:';
strList{initTimeIdx} = 'Initialization took:';

%Get pressure at the fault zone from Zeidouni's method for the averaged 
%injection layer
openfig('Analytical/analyticalD_average_pressurefault.fig', 'invisible');
data=get(gca,'Children');
xAnalyticalD_average=get(data,'XData'); %[years] get the x data (time)
%get the y data
%Perform a superposition such that after 50 years the injection rate
%becomes zero
%Dimensionless leakage rate for 100 years injection
yAnalyticalD_average100years=get(data,'YData'); %[Pa]
%Dimensioless extraction rate for 50 years
yAnalyticalD_averageExtraction50years= yAnalyticalD_average100years(xAnalyticalD_average<50+1e-9);
%Modifiy extraction rate fill entries of first 50 years with zeros and
%next 50 years with dimensionless extraction rate
yAnalyticalD_averageExtraction50years= ...
    [zeros([1 length(yAnalyticalD_averageExtraction50years)]) yAnalyticalD_averageExtraction50years];
%Superimpose(subtract) solution of 100 years injection with extraction rate
%for the last 50 years
yAnalyticalD_average=yAnalyticalD_average100years-yAnalyticalD_averageExtraction50years; %[Pa]
%Convert to bar for plot
yAnalyticalD_average=yAnalyticalD_average/1e5; %[bar]

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600; % in years
    flux{logIter}{fluxTotalIdx} = (output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx})) ...
        - output{fluxTotalIdx}(tInitIdx(logIter)))* (-1)/volInjectionRate;
    flux{logIter}{fluxFaultIdx} = (output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx}))...
        - output{fluxFaultIdx}(tInitIdx(logIter)))* (-1)/volInjectionRate;
    flux{logIter}{fluxHolesIdx} = (output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx}))...
        - output{fluxHolesIdx}(tInitIdx(logIter)))* (-1)/volInjectionRate;
    pressure{logIter}{M1Idx} = (output{M1Idx}(tInitIdx(logIter):length(output{M1Idx})) - output{M1Idx}(tInitIdx(logIter)))/1e5; % pressure in bar
    pressure{logIter}{M2Idx} = (output{M2Idx}(tInitIdx(logIter):length(output{M2Idx})) - output{M2Idx}(tInitIdx(logIter)))/1e5; % pressure in bar
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
Fig = figure();
set(gcf, 'Units', 'centimeters');
%specify color vector
co = [0 0 1;
      0 0.5 0;
      1 0 0;
%       0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';
  
%Map flux index to subplotiter
pressureIter = [M1Idx, M2Idx];

for subplotIter=1:2
    
    subplot(1,2,subplotIter);
    %Loop over the log-files
    for logIter = 1:length(logFileName)
        pressureplot = plot(time{logIter}(time{logIter}<=timePlot), ...
            pressure{logIter}{pressureIter(subplotIter)}(time{logIter}<=timePlot), sy{logIter}, 'color', co(logIter,:));
        set(pressureplot, 'LineWidth',1.5);
        if(pressureIter(subplotIter)==M1Idx)
            axis([0.0,100,-1,20])
        end
        hold on;
    end
    if(subplotIter==2)
        pressureplot = plot(xAnalyticalD_average, yAnalyticalD_average, sy{logIter+1}, 'color', co(logIter+1,:));
        axis([0.0,100,0.0,1.0])
    end
    h_leg = legend(legendStr, 'Location', 'NorthEast');
    set(h_leg,'Interpreter',strInterpreter, 'FontSize', tickFontSize);
    h_title = title(titel{subplotIter});
    set(pressureplot, 'LineWidth',1.5);
    set(h_title,'Interpreter',strInterpreter, 'FontSize', iFontSize)
    ylabel('Pressure buildup [bar]', 'FontSize', iFontSize, 'Interpreter',strInterpreter);
    xlabel('Time [Years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize)
    set(gca, 'XTick', [0, 20, 40, 60, 80, 100]);
    axis square;
end
    hold off;
    
    set(gcf, 'Units','centimeters', 'Position',[0 0 19 17])
    set(gcf, 'PaperPositionMode','auto')
    saveas(Fig,plotName{1},'epsc')
    %This function fixes the issue of badly exported dots in the line style
    fix_dottedline([plotName{1}, '.eps']);
