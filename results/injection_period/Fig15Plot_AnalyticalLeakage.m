%Calculate the volumetric leakage rates displaced over the individual vertical pathways
%into the target aquifers for all models - low diffuse leakage scenario
%k=1e-20 m2

clear all;
plotName{1} = 'Fig15_anal';

%define the indices for result and search lists
timeIdx = 1;
fluxTotalIdx = 2;
fluxFaultIdx = 3;
fluxHolesIdx = 4;
initTimeIdx = 5;

%Volumetric injection rate [m3/s], injection rate [kg/s]
injectionRate = 24.5136; % [kg/s] brine injection rate reference case
injectionDensity = 1078; % [kg/m3] initial brine density injection point reference case
volInjectionRate = injectionRate/injectionDensity; % [m3/s]

%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
eps = 1e4;
%array of logfiles to read
logFileName{1} = '2p3c_bar1e-20_wm0.log';
logFileName{2} = '1p2c_bar1e-20.log';
logFileName{3} = '1p1c_bar1e-20.log';
logFileName{4} = 'generic_bar1e-20.log';


%Leagage
% Get analytical solution from .fig for the averaged injection layer
openfig('Analytical/analyticalD_average_injectionrate.fig', 'invisible');
data=get(gca,'Children');
xAnalyticalD_average=get(data,'XData'); %[years] get the x data (time)
%get the y data
%Perform a superposition such that after 50 years the injection rate
%becomes zero

%Dimensionless leakage rate for 100 years injection
yAnalyticalD_average100years=get(data,'YData'); %[-]
%Dimensioless extraction rate for 50 years
yAnalyticalD_averageExtraction50years= yAnalyticalD_average100years(xAnalyticalD_average<50+1e-9);
%Modifiy extraction rate fill entries of first 50 years with zeros and
%next 50 years with dimensionless extraction rate
yAnalyticalD_averageExtraction50years= ...
    [zeros([1 length(yAnalyticalD_averageExtraction50years)]) yAnalyticalD_averageExtraction50years];
%Superimpose(subtract) solution of 100 years injection with extraction rate
%for the last 50 years
yAnalyticalD_average=yAnalyticalD_average100years-yAnalyticalD_averageExtraction50years;

%Leagage
% Get analytical solution from .fig for the not averaged injection layer
% (Solling only)
openfig('Analytical/analyticalD_noaverage_injectionrate.fig', 'invisible');
data=get(gca,'Children');
xAnalyticalD_noaverage=get(data,'XData'); %[years] get the x data (time)
%get the y data
%Perform a superposition such that after 50 years the injection rate
%becomes zero

%Dimensionless leakage rate for 100 years injection
yAnalyticalD_noaverage100years=get(data,'YData'); %[-]
%Dimensioless extraction rate for 50 years
yAnalyticalD_noaverageExtraction50years= yAnalyticalD_noaverage100years(xAnalyticalD_average<50+1e-9);
%Modifiy extraction rate fill entries of first 50 years with zeros and
%next 50 years with dimensionless extraction rate
yAnalyticalD_noaverageExtraction50years= ...
    [zeros([1 length(yAnalyticalD_noaverageExtraction50years)]) yAnalyticalD_noaverageExtraction50years];
%Superimpose(subtract) solution of 100 years injection with extraction rate
%for the last 50 years
yAnalyticalD_noaverage=yAnalyticalD_noaverage100years-yAnalyticalD_noaverageExtraction50years;


% %Pressure
% %Get pressure at the fault zone from Zeidouni's method for the averaged 
% %injection layer
% openfig('Analytical/analyticalD_average_pressurefault.fig', 'invisible');
% data=get(gca,'Children');
% xAnalyticalD_Press_average=get(data,'XData'); %[years] get the x data (time)
% %get the y data
% %Perform a superposition such that after 50 years the injection rate
% %becomes zero
% %Dimensionless leakage rate for 100 years injection
% yAnalyticalD_Press_average100years=get(data,'YData'); %[Pa]
% %Dimensioless extraction rate for 50 years
% yAnalyticalD_Press_averageExtraction50years= yAnalyticalD_Press_average100years(xAnalyticalD_Press_average<50+1e-9);
% %Modifiy extraction rate fill entries of first 50 years with zeros and
% %next 50 years with dimensionless extraction rate
% yAnalyticalD_Press_averageExtraction50years= ...
%     [zeros([1 length(yAnalyticalD_Press_averageExtraction50years)]) yAnalyticalD_Press_averageExtraction50years];
% %Superimpose(subtract) solution of 100 years injection with extraction rate
% %for the last 50 years
% yAnalyticalD_Press_average=yAnalyticalD_Press_average100years-yAnalyticalD_Press_averageExtraction50years; %[Pa]
% %Convert to bar for plot
% yAnalyticalD_Press_average=yAnalyticalD_Press_average/1e5; %[bar]
% 
% %Pressure
% %Get pressure at the fault zone from Zeidouni's method for the averaged 
% %injection layer
% openfig('Analytical/analyticalD_noaverage_pressurefault.fig', 'invisible');
% data=get(gca,'Children');
% xAnalyticalD_Press_noaverage=get(data,'XData'); %[years] get the x data (time)
% %get the y data
% %Perform a superposition such that after 50 years the injection rate
% %becomes zero
% %Dimensionless leakage rate for 100 years injection
% yAnalyticalD_Press_noaverage100years=get(data,'YData'); %[Pa]
% %Dimensioless extraction rate for 50 years
% yAnalyticalD_Press_noaverageExtraction50years= yAnalyticalD_Press_noaverage100years(xAnalyticalD_Press_average<50+1e-9);
% %Modifiy extraction rate fill entries of first 50 years with zeros and
% %next 50 years with dimensionless extraction rate
% yAnalyticalD_Press_noaverageExtraction50years= ...
%     [zeros([1 length(yAnalyticalD_Press_noaverageExtraction50years)]) yAnalyticalD_Press_noaverageExtraction50years];
% %Superimpose(subtract) solution of 100 years injection with extraction rate
% %for the last 50 years
% yAnalyticalD_Press_noaverage=yAnalyticalD_Press_noaverage100years-yAnalyticalD_Press_noaverageExtraction50years; %[Pa]
% %Convert to bar for plot
% yAnalyticalD_Press_noaverage=yAnalyticalD_Press_noaverage/1e5; %[bar]


titel{1} = 'Flow over fault zone'; %$k = 10^{-20}$ m$^2$';
legendStr{1} = 'Case i: Only Solling layer';
legendStr{2} = 'Case ii: Averaged';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Volume flow around salt wall [m3/s]:';
strList{fluxHolesIdx} = 'Volume flow across windows Rupel [m3/s]:';
strList{fluxTotalIdx} = 'Volume flow into target aquifers [m3/s]:';
strList{initTimeIdx} = 'Initialization took:';

%calculate the fluxes use the function outputReader for reading the
%log-files
% for logIter = 1:length(logFileName)
%     output = outputReader(strList, logFileName{logIter});
%     tinit(logIter) = output{initTimeIdx};
%     tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
%     time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600; % in years
%     flux{logIter}{fluxTotalIdx} = (output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx})) ...
%          - output{fluxTotalIdx}(tInitIdx(logIter)))* (-1)/volInjectionRate;
%     flux{logIter}{fluxFaultIdx} = (output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx})) ...
%          - output{fluxFaultIdx}(tInitIdx(logIter)))* (-1)/volInjectionRate;
%     flux{logIter}{fluxHolesIdx} = (output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx})) ...
%          - output{fluxHolesIdx}(tInitIdx(logIter)))* (-1)/volInjectionRate;
%     if(length(output{1}) ~= length(output{2}))
%         error('Different number of elements foound for search patterns');
%     end
% end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
Fig = figure();
set(gcf, 'Units', 'centimeters');
%specify color vector
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0
      0.5 0.5 0.5];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';
  
%Map flux index to subplotiter
fluxIter = [fluxHolesIdx, fluxFaultIdx];

   
 % Plot: numerical and analytical solutions
    
 for subplotIter=1:1
     
     subplot(1,2,subplotIter);
     
     if(subplotIter==1)
         fluxplot = plot(xAnalyticalD_noaverage, yAnalyticalD_noaverage, sy{2},  'color', co(6,:));
         axis([0,100,0,1.0])
         set(fluxplot, 'LineWidth',1.5);
                  hold on;
         fluxplot = plot(xAnalyticalD_average, yAnalyticalD_average, sy{1}, 'color', co(5,:));
         set(fluxplot, 'LineWidth',1.5);
     end
     
    if(subplotIter==2)
        pressureplot = plot(xAnalyticalD_Press_noaverage, yAnalyticalD_Press_noaverage,  sy{2}, 'color', co(6,:));
        axis([0.0,100,0.0,0.2])
        set(fluxplot, 'LineWidth',1.5);
        hold on;
        pressureplot = plot(xAnalyticalD_Press_average, yAnalyticalD_Press_average,  sy{1}, 'color', co(5,:));
        set(fluxplot, 'LineWidth',1.5);
    end
 end
 
 h_leg = legend(legendStr{1:2}, 'Location', 'NorthEast');
 set(h_leg,'Interpreter',strInterpreter, 'FontSize', 8);
 h_title = title(titel{subplotIter});
 set(h_title,'Interpreter',strInterpreter, 'FontSize', iFontSize)
 ylabel('Vol. flow/injection rate [-]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
 xlabel('Time [Years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
 %Set the font size of the axis ticks
 set(gca,'FontSize',tickFontSize)
 
 set(gca, 'XTick', [0, 20, 40, 60, 80, 100]);
 axis square;
 
 hold off;
 
 set(gcf, 'Units','centimeters', 'Position',[0 0 18 18])
 set(gcf, 'PaperPositionMode','auto')
 saveas(Fig,plotName{1},'epsc')
 %This function fixes the issue of badly exported dots in the line style
 fix_dottedline([plotName{1}, '.eps']);

