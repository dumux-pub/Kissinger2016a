%Plot the flux through the fault zone into the top aquifer for the complex geometry
%model using the outputReader.m.
% Scenario variable barrier permeability

clear all;
% plotName{1} = 'bar_1e17';
% plotName{2} = 'bar_1e18';
% plotName{3} = 'bar_1e19';
% plotName{4} = 'bar_1e20';
plotName = 'Fig12_varbar';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;
fluxLateralIdx = 6;
initTimeIdx = 7;

%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
eps = 1e4;
injectionRate = 24.9;
%array of logfiles to read
logFileName{1} = '1p2c/bar1e-17/out.log';
logFileName{2} = '1p2c/1p2c_reference/out.log';
logFileName{3} = '1p2c/bar1e-19/out.log';
logFileName{4} = '1p2c/bar1e-20/out.log';
titel{1} = 'K = 10$^{-17}$ m$^2$';
titel{2} = 'K = 10$^{-18}$ m$^2$';
titel{3} = 'K = 10$^{-19}$ m$^2$';
titel{4} = 'K = 10$^{-20}$ m$^2$';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Total mass flow around salt wall [kg/s]:';
strList{fluxRupelIdx} = 'Total mass flow across Rupel [kg/s]:';
strList{fluxHolesIdx} = 'Total mass flow across windows Rupel [kg/s]:';
strList{fluxTotalIdx} = 'Total mass flow into target aquifers [kg/s]:';
strList{fluxLateralIdx} = 'Total mass flow across lateral boundary [kg/s]:';
strList{initTimeIdx} = 'Initialization took:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600;
    fluxTotal{logIter} = (output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx})))*(-1)/injectionRate;
    fluxHoles{logIter} = (output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx})))*(-1)/injectionRate;
    fluxFault{logIter} = (output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx})))*(-1)/injectionRate;
    fluxLateral{logIter} = (output{fluxLateralIdx}(tInitIdx(logIter):length(output{fluxLateralIdx})))*(-1)/injectionRate;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 19, 17]); 
        %'PaperType','a4letter',...
        %'PaperOrientation','Portrait',...
        %'PaperUnits','centimeters', ...
        %'ActivePositionProperty','Position', ....
        %'PaperPosition',[0 , 0, 15.89, 15.89]); 
        %'PaperPosition',[2.54 , 2.54, 18.43, 18.43]); 
        
%colors
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';

for logIter = 1:length(logFileName)

    subplot(2,2,logIter);
    fluxplot = plot(time{logIter}(time{logIter}<=timePlot), fluxTotal{logIter}(time{logIter}<=timePlot), sy{1},...
        time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot),  sy{2}, ...
        time{logIter}(time{logIter}<=timePlot), fluxHoles{logIter}(time{logIter}<=timePlot),  sy{3});
    set(fluxplot, 'LineWidth',1.0);
    h_title = title(titel{logIter},'FontSize', iFontSize,'Interpreter',strInterpreter);
    ylabel('Brine flow / injection rate [-]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    xlabel('Time [years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    set(gca, 'XTick', [0, 20, 40, 60, 80, 100]);
    if(logIter==1)
        h_leg = legend('Total brine flow', ...
                       'Flow over fault zone', ...
                       'Flow over hydro. window','Location', 'NorthEast');
        set(h_leg,'Interpreter',strInterpreter, 'FontSize', 8);   
    end
    set(fluxplot, 'LineWidth',1.5);
    axis([0,100,-0.1,1])
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize);
    axis square;
%     grid on;   
end

%set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName,'epsc')
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName, '.eps']);
