# Simulation folders for the initialization period for the 1p2c model.

Each folder contains the input file for running the initialization period of a specific scenario (Low, Medium, and High) as explained in 4.2, Scenario study 1. The main simulation output is a dgf-grid where the primary variables for each degree of freedom are stored. These grids (containing the primary variables) are stored in *Kissinger2016a/grid* with their respective scenario names. The results for the injection period are calculated in *Kissinger2016a/results/injection_period* based on these grids.

**Files:**

- **reference** Medium or reference scenario

- **grad_1-0** Low scenario

- **grad_2-0** High scenario

